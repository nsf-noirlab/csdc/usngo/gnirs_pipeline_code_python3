![Stellar_banner](/examples/Stellar/Stellar_banner.jpg "Stellar_banner.jpg") *Credit:T.A. Rector (University of Alaska Anchorage) and H. Schweiker (WIYN and NOIRLab/NSF/AURA)*

This directory contains two stellar science cases used in the science verification process. The sources included are:

- [HH212B](https://gitlab.com/nsf-noirlab/csdc/usngo/gnirs_pipeline_code_python3/-/tree/main/examples/Stellar/HH212B)
- [WISE0410](https://gitlab.com/nsf-noirlab/csdc/usngo/gnirs_pipeline_code_python3/-/tree/main/examples/Stellar/wise0410)
