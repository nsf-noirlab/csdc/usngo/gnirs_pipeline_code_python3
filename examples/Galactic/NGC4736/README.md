# **NGC 4736**
## **GN-2011A-Q-126**

This example illustrates how you can modify the pipeline's code.

This source was included in the Gemini instrumentation website under [GNRIS data reduction](https://www.gemini.edu/instrumentation/gnirs/data-reduction). This observation was chosen for this example because it was a poor weather program. When ran through the pipeline, it fails to run with the preset parameters.

First, we will demonstrate how to make the pipeline interctive. Next, we will modify the data sheet to zoom in on the spectrum. When you try to run the data through the pipeline, you will be met with this error:

![Pipeline Error](/examples/Galactic/NGC4736c/auto_fail.png "Pipeline Error") *Pipeline Error*

Closer inspection of the error tells us that the pipeline is failing to find a wavelength calibrated spectrum for the source. Since the pipeline cannot do this independently, we will need to enable the interactive mode. To do this, first locate the directory that contains the pipeline's code, XDGNIRS_V2.2.6. Within this directory, we can locate the code respoinsible for wavelength calibration, namely SdistorAndWLcaliXD.py.

The wavelength calibration portion of the code, iraf.nswavelength() is located in Line 127. Within the long list of parameters is one called 'fl_inter' and it is set to 'no' by default. This is what we are looking for. By simply setting the parameter to 'yes', the pipeline is now able to allow the user to manually perform this step.

![fl_inter_no](/examples/Galactic/NGC4736/fl_inter_no.png "fl_inter_no.png") *By default, the interactive parameter is set to 'no'.*

![fl_inter_yes](/examples/Galactic/NGC4736/fl_inter_yes.png "fl_inter_yes.png") *fl_inter parameter needs to be set to 'yes' to enable interactive wavelength calibration.*

After setting fl_inter to 'yes', make sure to save the change and rerun the pipeline. It will appear to run like normal, but once its time for the wavelength calibration, the pipeline will display a prompt that looks like this:

![First interactive prompt](/examples/Galactic/NGC4736/interactive_1.png "interactive_1.png")  *This is the first interactive prompt that the pipeline will display.*

Since the pipeline was able to find autoidentify wavelength solutions this time, we can enter 'no'. Next, the pipeline will ask if you would like to refit the dispersion function interactively, to which we will respond 'NO' (this is a case sensitive respone). 

![Second interactive prompt](/examples/Galactic/NGC4736/interactive_2.png "interactive_2.png")  *This is the second interactive prompt that the pipeline will display.*

Repeat the process until you find the point where the pipeline cannot autoidentify a solution. Once this happens, the pipeline's output will look like this:

![Third interactive prompt](/examples/Galactic/NGC4736/interactive_3.png "interactive_3.png")  *This is the third interactive prompt that the pipeline will display.*

After responding 'yes' to this prompt, an IRAF window will be displayed which will allow you to manually identify the spectrum features. To accomplish this, you will need to move your cursor over the peaks and type 'm' to mark the peak. A yellow marker will appear over the peak and a white bar will appear on the bottom of the plot where you manually enter the wavelength. After marking a peak, the prompt will display the peak's wavelength in Angstroms. This is the value that you need to provide IRAF. Repeat this process for all of the peaks, then type 'q' to exit IRAF. This will return you to the pipeline's interactive prompt that will attempt to automatically find a solution for the remaining orders. 

![Fourth interactive prompt](/examples/Galactic/NGC4736/interactive_4.png "interactive_4.png")  *This is the fourth interactive prompt that the pipeline will display.*

By this point, the pipeline has what it needs to complete the process on its own. Once the code finishes running, you can move to the directory titled 'PRODUCTS' to review the final output. For a quick view of the reduced spectrum, you can check out the file labeled NGC4736_data_sheet.pdf.

![Data Sheet 1](/examples/Galactic/NGC4736/data_sheet_1.png "data_sheet_1.png")  *This is the first iteration of the data sheet that should be created after following the previous instructions. The purpose of this image is to get a first impression of the reduced spectrum. At the moment, this is not useful to us since the plot is zoomed too far out.*





Now let's adjust the y-limit in the resulting datasheet to better study the spectrum.

![Reduced Spectrum](/examples/Galactic/NGC4736/NGC4736_datasheet.png "NGC4736_datasheet.png") *NGC4736_datasheet.png*
