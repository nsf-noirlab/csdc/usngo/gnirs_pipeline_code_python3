# __**NGC1961**__

NGC1961 was observed on December 4th, 2012 ([GN-2012B-Q-112](https://archive.gemini.edu/searchform/cols=CTOWBEQ/notengineering/NotFail/not_site_monitoring/GN-2012B-Q-112)). This massive spiral galaxy is a Low-Ionization Nuclear Emission-Line Region (LINER) 2 source and is classified as a starburst galaxy (Condon et al. 1991) containing approximatly 5x10^10 M<sub>Sun</sub> of atomic hydrogen. Besides this galaxy's impressive morphologgy and dust lanes, this source is rich with history as it hosted several recent supernovae (Mason et al. 2015) and its warped disk is evidence of a past collision with another galaxy (Combes et al. 2009).

![NGC1961](/examples/Galactic/NGC1961/NGC1961_data_sheet.png "NGC1961 datasheet.png") *NGC1961 datasheet*

![NGC1961_pub](/examples/Galactic/NGC1961/NGC1961_hr.jpg "NGC1961_hr.jpg") *NGC1961 published spectrum*

 ## References
 - Combes, F., Baker, A. J., Schinnerer, E., et al. 2009, A&A, 503, 73
 - Condon, J. J., Frayer, D. T., & Broderick, J. J. 1991, AJ, 101, 362
 - R. E. Mason et al 2015 ApJS 217 13