![NGC4579](/examples/Galactic/NGC4579/NGC4579_data_sheet.png "NGC4579 datasheet.png") *NGC4579 datasheet*

![NGC4579_pub](/examples/Galactic/NGC4579/NGC4579_hr.png "NGC4579_hr.png") *NGC4579 published spectrum*
