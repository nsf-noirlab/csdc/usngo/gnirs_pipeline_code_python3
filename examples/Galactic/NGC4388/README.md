![NGC4388](/examples/Galactic/NGC4388/NGC4388_data_sheet.png "NGC4388 datasheet.png") *NGC4388 datasheet*

![NGC4388_pub](/examples/Galactic/NGC4388/NGC4388_hr.jpg "NGC4388_hr.jpg") *NGC4388 published spectrum*
