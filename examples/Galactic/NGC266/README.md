![NGC266](/examples/Galactic/NGC266/NGC266_data_sheet.png "NGC266 datasheet.png") *NGC266 datasheet*

![NGC266_pub](/examples/Galactic/NGC266/NGC266_hr.jpg "NGC266_hr.jpg") *NGC266 published spectrum*
