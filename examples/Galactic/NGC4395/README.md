![NGC4395](/examples/Galactic/NGC4395/NGC4395_data_sheet.png "NGC4395 datasheet.png") *NGC4395 datasheet*

![NGC4395_pub](/examples/Galactic/NGC4395/NGC4395_hr.jpg "NGC4395_hr.jpg") *NGC4395 published spectrum*
