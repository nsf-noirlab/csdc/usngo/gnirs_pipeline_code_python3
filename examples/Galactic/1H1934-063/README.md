# __**1H1934-063**__


1H1934-063 was observed on July 11th, 2013, and is the only Non-Palomar galaxy in our science verification sample. Located at a redshift of z=0.01059 (Rodriguez et al. 2008), this Narrow-Line Seyfert 1 Object contains a black hole mass M<sub>BH</sub> = 3 x 10^6 M<sub>sun</sub> (Rodríguez-Ardila et al. 2000). In the optical, this source's spectrum displays prominent forbidden lines such as [Fe x] and [Fe xiv]. In the near-infrared spectrum, coronal lines such as [S VIII], [Ca VIII], and [Si X] stand out. 

![1H1934-063](/examples/Galactic/1H1934-063/1H1934-063_data_sheet.png "1H1934-063 datasheet.png") *1H1934-063 datasheet*

![1H1934-063_pub](/examples/Galactic/1H1934-063/1H1934-063_hr.jpg "1H1934-063_hr.jpg") *1H1934-063 published spectrum*

 ## References
- Rodriguez, J., Tomsick, J., & Chaty, S. 2008, A&A, 482, 731
- Rodríguez-Ardila, A., Pastoriza, M. G., & Donzelli, C. J. 2000, ApJS, 126, 63
- Frederick S., Kara E., Reynolds C., Pinto C., Fabian A., 2018, Ap, 867, 67