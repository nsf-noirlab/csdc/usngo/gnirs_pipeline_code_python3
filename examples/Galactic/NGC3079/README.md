# __**NGC3079**__

NGC3079 was observed on December 1st, 2012 ([GN-2012A-Q-23](https://archive.gemini.edu/searchform/cols=CTOWBEQ/NotFail/not_site_monitoring/notengineering/20120123/GN-2012A-Q-23)).

![NGC3079](/examples/Galactic/NGC3079/NGC3079_data_sheet.png "NGC3079 datasheet.png") *NGC3079 datasheet*

![NGC3079_pub](/examples/Galactic/NGC3079/NGC3079_hr.jpg "NGC3079_hr.jpg") *NGC3079 published spectrum*