![NGC4450](/examples/Galactic/NGC4450/NGC4450_data_sheet.png "NGC4450 datasheet.png") *NGC4450 datasheet*

![NGC4450_pub](/examples/Galactic/NGC4450/NGC4450_hr.jpg "NGC4450_hr.jpg") *NGC4450 published spectrum*
