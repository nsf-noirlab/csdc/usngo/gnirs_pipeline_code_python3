# __**NGC2273**__ [How to shift spectrum to the rest frame]

NGC2273 was observed on December 4th, 2012 ([GN-2012B-Q-112](https://archive.gemini.edu/searchform/cols=CTOWBEQ/notengineering/NotFail/not_site_monitoring/GN-2012B-Q-112)).

Here we will demonstrate how to configure the pipeline's input so the final spectrum is shifted into the rest frame. To do this, we will make use of the 'shift_to_rest' option. By setting 'shift_to_rest='yes'', you have the choice of letting the pipeline search SIMBAD for the redshift, or you can manully define its redshift if you know it already. The pipeline can convert radial velocity to redshift if this is the only availble value on SIMBAD, or alternitavely, it can also just pull the spectroscopic redshift value.

First, we will run the pipeline normally to get the spectrum in the observation plane. To do this, I will be using the inputfiles.lst that is provided in this directory. To run the pipeline, enter this command in the directory with the data files:\n
./../XDGNIRS_V2.2.6/XDpiped.csh'


![NGC2273](/examples/Galactic/NGC2273/NGC2273_data_sheet.png "NGC2273 datasheet.png") *NGC2273 datasheet*

![NGC2273_pub](/examples/Galactic/NGC2273/NGC2273_hr.jpg "NGC2273_hr.jpg") *NGC2273 published spectrum*

Now that we have the unshifted spectrum, we will now enter the following command (also in the directory where your data files are stored)
./../XDGNIRS_V2.2.6/XDpiped.csh shift_to_rest='yes'




 ## References
 -
 -
 - 



