NGC3031 (also known as M81) is the galaxy that is examined in XDGNIRS_v226.pdf.

![NGC3031](/examples/Galactic/NGC3031/NGC3031_data_sheet.png "NGC3031_data_sheet.png") *NGC3031_datasheet obtained using the provied inputfiles.lst*

![NGC3031_manual](/examples/Galactic/NGC3031/NGC3031_manual.png "NGC3031_manual.png") *Data sheet included in XD GNIRS Manual*