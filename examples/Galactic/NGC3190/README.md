# __**NGC3190**__

NGC3190 was observed on November 27th, 2011 ([GN-2011B-Q-111](https://archive.gemini.edu/searchform/cols=CTOWBEQ/NotFail/not_site_monitoring/notengineering/20111127)).

![NGC3190](/examples/Galactic/NGC3190/NGC3190_data_sheet.png "NGC3190 datasheet.png") *NGC3190 datasheet*

![NGC3190_pub](/examples/Galactic/NGC3190/NGC3190_hr.jpg "NGC3190_hr.jpg") *NGC3190 published spectrum*
