#!/usr/bin/python
#
#Look up target redshift
#
# Import libraries
#

import sys
from astroquery.simbad import Simbad
import astropy.coordinates as coord
import astropy.units as u
from astropy.table import Table


#Construct URL based on science target coords, execute SIMBAD query to find spectral type
name = str(sys.argv[1:2][0])
Simbad.add_votable_fields('flux(K)', 'sp', 'rvz_radvel', 'rvz_type')
star_table = Simbad.query_region(name, radius=0.01 * u.deg)
print( star_table)
Rvel = str(star_table['RVZ_RADVEL'][0])
Type = star_table['RVZ_TYPE'][0]

try:
    #if Rvel can be converted to float, is probably real
    testvar = float(Rvel)
    if Type == 'z':
        Rvel = Rvel
    else:
        #If Type=v then it's a velocity; could convert but too lazy to do that here
        print( 'HERE IS YOUR REDSHIFT (1)', Type)
        Rvel = 'Warning:_no_redshift_found,_not_shifting_to_rest_frame'
        
except:
    print( 'HERE IS YOUR REDSHIFT (2)', Rvel)
    Rvel = 'Warning:_no_redshift_found,_not_shifting_to_rest_frame'

redshiftfile = sys.argv[2:3][0]
zfile = open(redshiftfile,'w')  
zfile.write (Rvel+'\n')  
zfile.close()
    


