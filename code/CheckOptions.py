
import sys

#Check numerical parameters
entry = sys.argv[1]
exit = sys.argv[2]
tgt_thresh = sys.argv[6]
std_thresh = sys.argv[7]
aperture = sys.argv[21]
stepsize = sys.argv[23]
#Looking for things that aren't numbers
try:
    int(entry)
    int(exit)
    int(tgt_thresh)
    int(std_thresh)
    #I suppose these could also be floats, but let's just do this for now...
    int(aperture)
    int(stepsize)
except:
    print( "ERROR: 'start', 'stop', 'tgt_thresh', 'std_thresh', 'aperture' and 'columns' options must be integers. Looks like you've entered something that doesn't make sense. Please check and try again.")
    sys.exit()
#Looking for disallowed numbers
if int(entry) <0 or int(entry) > 13:
    print( "ERROR: Allowed values of 'start' are 0-13. You have entered", entry, ". Please try again.")
    sys.exit()
if int(exit) <0 or int(exit) > 13:
    print( "ERROR: Allowed values of 'stop' are 0-13. You have entered", exit, ". Please try again.")
    sys.exit()
if int(exit) < int(entry):
    print( "ERROR: 'start' must be <= 'stop'. You have entered", entry, "and", exit, ". Please try again.")
    sys.exit()

#Check yes/no options
yes_no = {"nsflat_inter":sys.argv[3], "despike_tgt":sys.argv[4], "despike_std":sys.argv[5], "nssdist_inter":sys.argv[8], "nswav_inter":sys.argv[9], "nsext_inter":sys.argv[11], "continuum_inter":sys.argv[12], "telluric1_inter":sys.argv[13], "use_apall":sys.argv[20],"extras":sys.argv[22], "deredshift":sys.argv[25], "ureka":sys.argv[28], "error_spectrum":sys.argv[30], "telluric2_inter":sys.argv[31]}
for key,value in yes_no.items():
    if value != "yes" and value != "no":
        print( "ERROR: Allowed values of '", key,"'are yes/no. You have entered", value, ". Please try again.")
        sys.exit()

#Check gemiraf option
gemiraf = sys.argv[27]
if gemiraf != "1.11.1" and gemiraf !="1.12+":
    print( "ERROR: allowed values of 'gemiraf_ver' are 1.11.1/1.12+. You have entered", gemiraf, ". Please try again.")
    sys.exit()

#Check cleanir_*flag options
for i in [14,15,16,17,18]:
    cleanir_flag = []
    for j in range (0, len(sys.argv[i])):
        cleanir_flag.append(sys.argv[i][j])
        allowed_flags = (["n","f","r","q"])
        for flag in cleanir_flag:
            if flag not in allowed_flags:
                print( "ERROR: allowed values of 'cleanir_*flag' are nfrq. You have entered", cleanir_flag, ". Please try again.")
                sys.exit()

#Check hlines option
hlines = sys.argv[24]
allowed_values = (["vega", "linefit_auto", "linefit_manual", "vega_tweak", "linefit_tweak", "none"])
if hlines not in allowed_values:
    print( "ERROR: allowed values of 'hlines' are vega/linefit_auto/linefit_manual/vega_tweak/linefit_tweak/none. You have entered", hlines, ". Please try again.")
    sys.exit()

#Check offsets option
offsets = sys.argv[29]
allowed_values = (["no", "manual"])
if offsets not in allowed_values:
    print( "ERROR: allowed values of 'offsets' are no/manual. You have entered", offsets, ". Please try again.")
    sys.exit()

#Remaining paramter is rawdir - user will have to figure that one out on their own.

    
