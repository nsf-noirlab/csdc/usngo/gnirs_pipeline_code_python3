#!/usr/bin/env python

##########################################################################
#
# author: J.Miller
# 
# usage: query.py <date> <...>  <...>
#
#     <date>                   date in Hilo after night (i.e. UT-date)
#     <...>                 ...
#     <...>                 ...
#  
# 2015.08.25 V0.1: (jmiller) Initial version from Jen 
#                  Query FITS database to find all GNIRS XDdata for each grating
#                  Download all XD data
#                  Query the program IDs from FITS header
#                  Find the list of unique ObsIDs
#                  Determine completeness of program
#                  Create a directory for each program 
#                  Create a 'raw' directory in each program directory	
#                  Move data into the appropriate directory
#                  Create input file list for reduction script
#                  Run script in top level program dir
# 2015.08.31 V0.2: (mpohlen) Added header. Added 'present' to jsonfilelist
# 2015.09.01 V0.3: (mpohlen, jmiller) Made some more modifications, debug
#                  utDate input, new working Dir, md5 change, download
#                  from fits or archive, avoid download if files present  
# 2015.09.02 V0.4: (mpohlen, jmiller) Rewrite selection of science/cals/acq  
#                  Add version of pipeline code 
# 2015.09.03 V0.5: (mpohlen, jmiller) Add logfile, webpage update, email in 
# 2015.09.04 V0.6: (jmiller, mpohlen) Allow 3rd codeversion, clean up code 
#                   Use mock std_star.txt for all three codeversion. 
# 2015.09.06 V0.7: (mpohlen) Add html page to copy_web, replaced all prints 
#                  with printLs
# 2015.09.07 V0.8: (mpohlen) Fix bug in logging email sending 
#                  Rename .html to .ihtml to make it visible (html blocked)
# 2015.09.09 V0.9: (mpohlen) Fix bug in logging email sending, again :-(
# 2015.09.15 V1.0: (jmiller) Rewrite webupdatelist so that the webpage will 
#                  only update if needed, fix shutil.copytree arg to fix 
#                  symbolic link problem
# 2015.09.15 V1.1: (jmiller,mpohlen) Moved email outside successful run if
#                  Fix get_cals, fix subprocess call, prevent overwrite 
#                  of webpage in debug mode 
# 2015.09.22 V1.2: (jmiller,mpohlen) Minor changes        
# 2015.09.22 V1.3: (jmiller) Change webUpdateList so that script looks for 
#                  necessary files created for 'successful' run. If programs
#                  are already on the webpage and cron is run again, update,  
#                  but do not append line.
# 2015.09.30 V1.4: (jmiller,mpohlen) Disable the scp to the webserver 
# 2015.10.01 V1.5: (jmiller) Clean up code; move necessary print statements 
#                  to logFile, delete unecessary print statements, etc...
# 2015.10.09 V1.6: (jmiller) Copy all data to allData even if the run was 
#                  unsuccessful. This will allow Rachel to troubleshoot
#                  problems with the pipeline.
# 2015.10.02 V1.7: (jmiller) Put in option (-o) to run code for specific OBSIDs
# 2015.10.12 V1.8: (jmiller,mpohlen) Fix option (-o), re-arrange move_temp,
#                  fix overwriting of webpage entry if same date run again 
# 2015.10.14 V1.9: (jmiller,mpohlen) address variable sky issue for sci and tell
# 2015.10.16 V2.0: (mpohlen) Fix bug 
# 2015.10.18 V2.1: (mpohlen) Now with proper fix :-(  
# 2015.10.28 V2.2: (jmiller,mpohlen) Fix checkVarSkies for telluric replace
# 2015.10.30 V2.3: (jmiller,mpohlen) Use second science image to look for cals. 
# 2015.11.04 V2.4: (jmiller,mpohlen) Run CheckSkies multiple times 
# 2015.11.06 V2.5: (mpohlen) Add present to fits server calls for calibrations
#                  Fixed bug in acq/acqCal search definition
# 2015.11.12 V2.6: (jmiller,mpohlen) Fix another bug in acq search definition
# 2015.11.13 V2.7: (mpohlen) Make the above a proper fix   
# 2015.11.17 V2.8: (jmiller,mpohlen) Quick fix another bug for ENG programs, 
#                  remove Rachel from email list for now 
# 2016.01.06 V2.9: (mpohlen) Add new code version and set as default
# 2016.01.07 V3.0: (mpohlen) Add more timestamps in logfile; Setting XDGNIRSDIR
#                  from within script; added XDGNIRS_v22_beta and set default
#                  Made code switching easier. Replaced web link with local 
#                  link on webpage until webserver issue is solved. 
# 2016.01.07 V3.1: (mpohlen) Only include 32 l/mm @  1.65 um; select second arc
#                  if available; recheck acq after trimming sci sequence to 
#                  catch re-acquisition cases where second sci set is larger;
#                  Added check so only A or B offset positions are selected
# 2016.01.08 V3.2: (mpohlen) Switch to 'new' pipeline version v221; if qOffset
#                  shows no sky check for pOffsets; catch 'None' in ctrlwvl
# 2016.01.11 V3.3: (mpohlen) Fix bug in copy over to allData if there is already
#                  data in there. 
# 2016.01.12 V3.4: (mpohlen) Switch to 'new' pipeline version v222; Fix bug 
#                  in checkSkies with re-checking acq.; disable copy to allData
#                  for debug run
# 2016.01.13 V3.5: (mpohlen) Switch to 'new' pipeline version v223
# 2016.01.14 V3.6: (mpohlen) Switch to 'new' pipeline version v224
# 2016.01.31 V3.7: (mpohlen) Switch to 'new' pipeline version v225; make 32lXD
#                  default (==only) disperser until pipeline can handle others
# 2016.02.02 V3.8: (mpohlen) Switch to 'new' pipeline version v226
# 2016.02.29 V3.9: (mpohlen) Add AN into email distribution 
# 2016.05.17 V4.0: (jmiller,mpohlen) Select only science with same #coadds/Texps
#                  Fix issue with not selecting ENGs and missing ctrwvl
# 2017.07.11 v4.1: (jmiller) Remove AN from emails 
# 2018.05.28 V4.2: (jmiller) Remove MP from emails
# 2021.04.27 v4.3: (jmiller) Change JM email address to noirlab
#
#        =====> Note, scp to webserver is disabled !!! 
#
version="2017.07.11 V4.2"
#
##########################################################################

## Local settings:
webDir='/sciops/instruments/gnirs/pipeOutput/'

## Import stuff: 

import datetime
import fnmatch
import glob
import hashlib
import json
import numpy
import os.path
import os, sys
import pyfits
import re
import shutil
import smtplib
import subprocess
import time
#import urllib2
import urllib.request as urllib2

from astrodata import AstroData
from optparse import OptionParser
from xml.dom.minidom import parseString


########################################################################### 
########################################################################### 

Def main(options, args):
  
   ## Grab current date:
   now = datetime.datetime.now()
   dateOfNight = now.strftime("%Y%m%d")
   timeString = now.strftime("%Y%m%d_%HH%MM%SS")

   ## Asign input parameters:  
   localRun = options.localRun
   obsidRun = options.obsidRun
   codeVersion = options.codeVersion 
   mailTo = options.email
   debug = options.debug
   runPipe = options.runPipe
   dispList = [options.dispList]
   if len(dispList[0]) == 0 :
    dispList= ['32lXD', '10lXD', '111lXD']
   if dispList[0] == '32':
      dispList= ['32lXD'] 
   if dispList[0] == '10':
      dispList= ['10lXD'] 
   if dispList[0] == '111':
      dispList= ['111lXD'] 

   if len(args) > 0:
     dateOfNight = args[0]
     
   if debug:
     print( 'Options =', options)
     print( 'Args =', args)

   sendEmail = True
   if localRun or debug:
     sendEmail = False

   ## Get main directory name:
   masterDir = '/home/irafuser/XDGNIRS_V2.2.6'
   resultDir = masterDir+'/allResults/'
   allDataDir = masterDir+'/allData/'

   ## Make the instrument temp directory the local directory
   tempDir = masterDir+'/temp/'
   if len(glob.glob(tempDir+'/')) == 0:
      os.mkdir(tempDir)
   else: 
      if debug:
        pass
      else:
        shutil.rmtree(tempDir) 
        os.mkdir(tempDir)
   os.chdir(tempDir)

   # Set code version:

   stdCodeNaming = ['v221', 'v222', 'v223' , 'v224', 'v225','v226']
   if codeVersion == 'v20a':
     codePath = '/home/gnda/gnirsPipe/XDGNIRS_v2.0a/XDpiped.csh'
   elif codeVersion == 'v20b':
     codePath = '/home/gnda/gnirsPipe/XDGNIRS_v2.0b/XDpiped.csh'
   elif codeVersion == 'v21beta':
     codePath = '/home/gnda/gnirsPipe/XDGNIRS_v21_beta/XDpiped.csh'
   elif codeVersion == 'v21betamerged':
     codePath = '/home/gnda/gnirsPipe/XDGNIRS_v21_beta_merged/XDpiped.csh'
   elif codeVersion == 'v22beta':
     codePath = '/home/gnda/gnirsPipe/XDGNIRS_v22_beta/XDpiped.csh'
   elif codeVersion in stdCodeNaming:
     codePath = '/home/irafuser/XDGNIRS_'+codeVersion+'/XDpiped.csh'
   else:
     logString = 'Code name not defined !' +'\n'
     printL(logString,debug)
     sys.exit()

   codeOld = ['v20a','v20b']
   codeNew = ['v21beta','v21betamerged','v22beta']+stdCodeNaming

   if codeVersion in codeOld:
     dataDir = '/OUTPUTS/'
   else:
     dataDir = '/PRODUCTS/'

   # Set XDGNIRSDIR variable: 
   os.environ['XDGNIRSDIR'] = codePath[:-11]

   # Create logfile:
   if debug:
     logFileName  = "/home/irafuser/XDGNIRS_V2.2.6/logFiles/debug_queryRun_"+ dateOfNight + "_" + timeString+ ".dat"
   else:
     logFileName  = "/home/irafuser/XDGNIRS_V2.2.6/logFiles/queryRun_"+ dateOfNight + "_" + timeString+ ".dat"
   global logFile
   logFile = open(logFileName,'w')


   ## Printout header: 
   infoStart = timeString+" Checking: "+dateOfNight+ '\n'+ '-------------------------------------' + '\n'
   if debug:
     logString = infoStart
     printL(logString,debug)     
   hline="++++++++++++++++++++++++++++++++++++++++++"+'\n'
   logFile.write(hline)
   logFile.write("(query.py version: "+str(version) +')\n')
   logFile.write("(Pipeline version: "+str(codePath) +')\n')
   logFile.write(hline)
   #logFile.write("(Call: "+ pyCall+')\n')
   logFile.write("\n"+'\n'+infoStart+"\n"+"\n")
   #
   logString = 'dispersr list: ' + str(dispList)+'\n'
   printL(logString,debug)

   # Look at each disperser to find XD GNIRS data  
   
   allValidObsidsInfo = {}

   global allTargetNames

   allTargetNames = {} 

   for d in dispList:
      logString = '======================================================' + '\n'
      logString = logString + '======================================================' + '\n'
      logString = logString + 'Checking: ' +  d+ "\n"
      printL(logString,debug)

      ## Get filelists for each obsid:        
      obsids, obsidsFileList, targetNames = get_obsids(d, dateOfNight, obsidRun, debug)
      allTargetNames.update(targetNames) 

      obsidsInfo  = get_filelists(dateOfNight, obsids, obsidsFileList, debug)

      ## Select valid obsids and get all fits files: 

      validObsidsInfo = valid_fetch(obsidsInfo,debug)

      ## Check all science frames have same #coadds/Texps:  
     
      validObsidsInfo = checkCoaddsTexps(validObsidsInfo, tempDir, dateOfNight,debug)
      validObsidsInfo = checkFileLists(validObsidsInfo,debug)

      ## Check for variably spaced skies: 

      validObsidsInfo = checkVarSkies(validObsidsInfo, tempDir, dateOfNight,debug)
      validObsidsInfo = checkFileLists(validObsidsInfo,debug)
      allValidObsidsInfo.update(validObsidsInfo)

      ## Fix acq. header to match science (needed for blind offset):

      fixHeader(validObsidsInfo,obsidsInfo,tempDir,debug)

      ## Create directories for selected obsids and copy files :

      create_copy(validObsidsInfo,tempDir, codeVersion,codeOld,codeNew,debug)

      ## Delete original files in temp :
 
      for myFile in glob.glob(tempDir+'/*.fits'):
           os.remove(myFile)
     
      logString = '======================================================' + '\n'
      logString = logString + '======================================================' + '\n'
      printL(logString, debug)

   ## Run GNIRS pipeline on files obids:

   webUpdateList = []
   for obsid in allValidObsidsInfo:
         targetName = allTargetNames[obsid]
         subDir = tempDir + obsid + '/'
         os.chdir(subDir)
         if debug:
           print( os.getcwd())
         cmd = codePath+' inputfiles.lst rawdir='+subDir+'raw/ ureka=no'
         if runPipe:
            now = datetime.datetime.now()
            nowString = now.strftime("%Y%m%d_%HH%MM%SS")
            logString = 'Start pipeline run @'+nowString  +': '+'\n'
            logString = logString + cmd +'\n'
            printL(logString, debug)
            p = subprocess.Popen(cmd, shell=True)
            while p.poll() is None:
               time.sleep(1)
            if p.poll() == '1':
               logString = ''+'\n'
               logString = logString + 'GNIRS pipeline run of '+obsid+ ' crashed!' +'\n'
               logString = logString + ''+'\n'
               printL(logString, debug)
            else:
               pdfFile =  tempDir+obsid+'/PRODUCTS/'+targetName+'_data_sheet.pdf'
               fitsFile = tempDir+obsid+'/PRODUCTS/'+targetName+'.fits'
               txtFile =  tempDir+obsid+'/PRODUCTS/'+targetName+'.txt'
               if debug: 
                  print( pdfFile +'\n' + fitsFile + '\n' + txtFile + '\n')
                  print( len(glob.glob(pdfFile)))
                  print( len(glob.glob(fitsFile)))
                  print( len(glob.glob(txtFile)))
               if len(glob.glob(pdfFile)) + len(glob.glob(fitsFile)) + len(glob.glob(txtFile)) != 3:
                  logString =  ''+'\n'
                  logString = logString + 'Necessary files for ' +obsid+ ' were not created. GNIRS pipeline Failed!' +'\n'
                  printL(logString, debug)
               else:
                  logString = ''+'\n'
                  logString = logString + 'GNIRS pipeline run of '+obsid+ ' successful!' +'\n'
                  logString = logString + ''+'\n'
                  printL(logString, debug)
                  webUpdateList.append([obsid,dateOfNight,targetName,timeString])
         else:
            logString = cmd
            logString = logString + 'Pipeline run skipped (option -r used)' +'\n'
            printL(logString, debug)

   ## Check timing:

   now = datetime.datetime.now()
   nowString = now.strftime("%Y%m%d_%HH%MM%SS")
   logString =  ''+'\n'
   logString = logString + 'Pipeline section finished at '+nowString 
   printL(logString, debug)

   ## Update webpage and save results for successful runs: 

   if len(webUpdateList) == 0:
      logString =  ''+'\n'
      logString = logString + 'No successful pipeline data. Skipping web update.' + '\n'
      logString = logString + 'Data can still be found in the allData directory'
      printL(logString, debug)
   else:
      ## Update webpage with results from successful runs: 
      #if runPipe:
      update_webpage(webUpdateList, timeString, resultDir,debug)

      ## Copy data to allResults:   

      copy_results(webUpdateList, tempDir, dataDir, resultDir, debug) 

      ## Copy data and webpage to webserver: 

      copy_webpage(webUpdateList, webDir, resultDir, debug)

   ## Copy temp directory to allData:

   move_temp(dateOfNight, tempDir, allDataDir, timeString,debug)

   ### Gather info into an email abd close logfile:
   if sendEmail:
       title = 'GNIRS daily XD pipeline run '+ dateOfNight
       logString = '\n'
       logString = logString + 'Sending email with logFile: ' + str(mailTo) + ' ' + title + '\n'
       printL(logString,debug)
       now = datetime.datetime.now()
       nowString = now.strftime("%Y%m%d_%HH%MM%SS")
       logString = '\n'
       logString =logString + '++++++++++++++++++++++++++++++++++++++++++' + '\n'

       logString = logString + 'query.py run finished at ' + nowString + '\n'
       logString =logString + '++++++++++++++++++++++++++++++++++++++++++' + '\n'
       printL(logString,debug)
       logFile.close()

       ## Close logfile:
       logFile.close()

       f = open(logFileName, 'r')
       emailTxt = f.read()
       f.close()
       sendMail (mailTo,'jmiller@gemini.edu',title,emailTxt)

   else:
        ## Close logfile:
       now = datetime.datetime.now()
       nowString = now.strftime("%Y%m%d_%HH%MM%SS")
       logString = '\n'
       logString =logString + '++++++++++++++++++++++++++++++++++++++++++' + '\n'

       logString = logString + 'query.py run finished at ' + nowString + '\n'
       logString =logString + '++++++++++++++++++++++++++++++++++++++++++' + '\n'
       printL(logString,debug)
       logFile.close()
 
    

############################################################################
############################################################################
############################################################################
############################################################################

## Some definitions: 
#--------
def checkPattern(qOffsets, pos,debug):
         abba = False
         abab = False
         aba = False 
         if qOffsets[pos] == qOffsets[pos+3]:
           if qOffsets[pos] ==  qOffsets[pos+2]:
             aba = True
           elif qOffsets[pos+1] ==  qOffsets[pos+2]:   
             abba = True
           else:
             printL('Pattern not defined',debug)
         elif qOffsets[pos] == qOffsets[pos+2] and qOffsets[pos+1] == qOffsets[pos+3]:
            abab = True
         else:
            printL('Pattern not defined',debug)
         if debug:
           print( abba, abab, aba)
         return abba, abab, aba

#--------
# Check that each 'image' has at least one 'sky':
def checkForSky(qOffsets,debug):
      fileNameList =  sorted(qOffsets.keys())
      if debug:
       print( 'filelist at start: ', fileNameList)
      goodImages = []
      for pos in range(len(fileNameList)): 
        if pos == 0:
          if debug:
            print( 'First', qOffsets[fileNameList[pos]], ' next', qOffsets[fileNameList[pos+1]])
          if abs(qOffsets[fileNameList[pos]] - qOffsets[fileNameList[pos+1]]) > 0.1:
            if debug:
               print( 'image added')
            goodImages.append(fileNameList[pos])
          else:
            if debug:
               print( 'image NOT added')
        elif pos < len(fileNameList) -1 :
          if debug:
            print( 'pos=',  pos, qOffsets[fileNameList[pos]], ' next', qOffsets[fileNameList[pos+1]], ' prev ', qOffsets[fileNameList[pos-1]])
          if abs(qOffsets[fileNameList[pos]] - qOffsets[fileNameList[pos+1]]) > 0.1  or  abs(qOffsets[fileNameList[pos]] - qOffsets[fileNameList[pos-1]]) > 0.1:
            if debug:
               print( 'image added')
            goodImages.append(fileNameList[pos])
          else:
            if debug:
               print( 'image NOT added')
        else: 
          if debug:
               print( 'Last',  qOffsets[fileNameList[pos]], ' next', qOffsets[fileNameList[pos-1]])
          if abs(qOffsets[fileNameList[pos]] - qOffsets[fileNameList[pos-1]]) > 0.1:
            if debug:
               print( 'image added')
            goodImages.append(fileNameList[pos])
          else:
            if debug:
               print( 'image NOT added')
      if debug:
       print( 'filelist at end: ', goodImages, '\n')
      return goodImages

#--------
def checkTiming(validObsidsInfo, validObsid, checkFilesPos, tempDir, dateOfNight, debug):

      # Get header info for all sci files of an obsid: 
      sciImages = []
      timings = []
      for sciFileName in validObsidsInfo[validObsid][checkFilesPos]: 
        adSci= AstroData(sciFileName)
        coadd = adSci.phu_get_key_value("COADDS")  # # of coadded images 
        expTime = adSci.phu_get_key_value("EXPTIME") #integration time (seconds)
        sciImages.append(sciFileName)
        printL(sciFileName+' (coadds/Texp): ' +str(coadd)+ '/'+str(expTime),debug)
       
        timings.append((coadd,expTime))
        #timings.append(expTime)
      #print sciImages,coadds ,expTimes

      #Create dictionary to find frequency: 
      d = {x:timings.count(x) for x in timings}
      timing, frequency = d.keys(), d.values()
      #Find most common setup:  
      max_index = frequency.index(max(frequency))
      #Select setup:
      selCoadd,selTexp = timing[max_index]
      #print selCoadd,selTexp

      #Go through list again and downselect:
      goodImages  = []
      badImages = []
      for i in range(len(sciImages)): 
        if (timings[i][0] == selCoadd) and (timings[i][1] == selTexp):
           goodImages.append(sciImages[i])
        else:
           badImages.append(sciImages[i])

      #Add info to logfile: 
      printL('',debug)
      numRemoved = len(badImages)
      if numRemoved > 0:
         printL('#coadds/Texp changed partway through the observation' + '\n',debug)
         
         printL(str(badImages) +' removed ' + '\n',debug)
      else:
         printL('#coadds/Texp all the same, so OK' + '\n',debug)
      printL('' ,debug)


      #Update input list with (in case) reduced filelist: 
      validObsidsInfo[validObsid][checkFilesPos] = goodImages

      return validObsidsInfo

#--------
# includes checking of 'each image has a sky' as well as 'variably spaced skies'
def checkSkies(validObsidsInfo, validObsid, acqFilePos, checkFilesPos, tempDir, dateOfNight, debug):
      if debug:
         print( 'acqFilePos, checkFilesPos', acqFilePos, checkFilesPos, '\n')
      acqFileName = tempDir+validObsidsInfo[validObsid][acqFilePos][0]
      adAcq= AstroData(acqFileName)
      pZero = adAcq.phu_get_key_value("POFFSET")
      qZero = adAcq.phu_get_key_value("QOFFSET")
      myCounter = 0
      timeDiff = 0
      pOffsets = {}
      qOffsets = {}
      timeDiffs = {}
      timeStamps = {}

      # Get header info for all sci files of an obsid: 
      for sciFileName in validObsidsInfo[validObsid][checkFilesPos]:
        # Get offset and time information from images: 
        adSci= AstroData(sciFileName)
        pOffset = round(adSci.phu_get_key_value("POFFSET") - pZero,1)
        qOffset = round(adSci.phu_get_key_value("QOFFSET") - qZero,1)
        pOffsets[sciFileName] = pOffset
        qOffsets[sciFileName] = qOffset
        dateObs = adSci.phu_get_key_value("DATE-OBS")
        timeObs = adSci.phu_get_key_value("TIME-OBS")
        timeStamp = datetime.datetime.strptime(dateObs + timeObs , '%Y-%m-%d%H:%M:%S.%f')
        timeStamps[sciFileName] = (timeStamp)
        # Calculate time difference between successive images: 
        if myCounter > 0:
          timeDiff = timeStamp - prevTimeStamp

        prevTimeStamp = timeStamp
        if myCounter > 0:
           if debug:
             print  ("%s %4.1f %4.1f %s %s %6.1f" % (sciFileName, pOffset, qOffset, dateObs, timeObs, round(timeDiff.total_seconds(),0)))
           else:
             logString = sciFileName + ' ' + str(pOffset)+ ' ' + str(qOffset)+ ' ' +  dateObs+ ' ' +  timeObs + ' ' + str(round(timeDiff.total_seconds(),0)) 
             printL(logString,debug)

        else:
           if debug:  
             print  ("%s %4.1f %4.1f %s %s %6.1f" % (sciFileName, pOffset, qOffset, dateObs, timeObs, float(timeDiff)))
           else:
             logString = sciFileName + ' ' + str(pOffset)+ ' ' + str(qOffset)+ ' ' +  dateObs+ ' ' +  timeObs + ' ' + str(float(timeDiff)) 
             printL(logString,debug)
        if myCounter > 0:
          timeDiffs[sciFileName] = (round(timeDiff.total_seconds(),0))
        else:
          timeDiffs[sciFileName] = timeDiff
        myCounter = myCounter + 1

      ## Check that each 'image' has at least one 'sky':

      printL('\n'+ 'Check that each "image" has at least one "sky": ' + '\n',debug)

      goodImages = checkForSky(qOffsets,debug) 

      if len(goodImages) == 0:
            if debug:
              print( 'No skies in Q, so looking for P')
            goodImages = checkForSky(pOffsets,debug) 

      if debug:
        print( goodImages)
      sciFileNames = validObsidsInfo[validObsid][checkFilesPos]
      newTimeDiffs = {}
      for entry in range(len(goodImages)):
           if entry == 0 : 
              newTimeDiffs.update({goodImages[entry]:0.0})
              prevTimeStamp = timeStamps[goodImages[entry]]
           else:
              newTimeDiff = timeStamps[goodImages[entry]] - prevTimeStamp
              newTimeDiffs.update({goodImages[entry]:round(newTimeDiff.total_seconds(),0)})
              prevTimeStamp = timeStamps[goodImages[entry]]

      if debug:
        print( 'newTimeDiffs: ', newTimeDiffs)

      ## Check timing:

      printL('\n' + 'Check timing: ' + '\n',debug)

      newTimeDiffsTmp = {key: value for key, value in newTimeDiffs.items() if value != 0.0}
      newTimeDiffMin = newTimeDiffs[min(newTimeDiffsTmp, key=newTimeDiffs.get)]
      newTimeDiffMax = newTimeDiffs[max(newTimeDiffs, key=newTimeDiffs.get)]
      if debug: 
        print( 'newTimeDiffMin, newTimeDiffMax:', newTimeDiffMin, newTimeDiffMax)

      fileNameMax = max(newTimeDiffs, key=newTimeDiffs.get)
      maxPos = goodImages.index(fileNameMax)
      if debug: 
        print(  'maxPos, fileNameMax:', maxPos, fileNameMax )

      newSciFileNames2 = []

      if 2 * newTimeDiffMin < newTimeDiffMax:
         #Check position of images which sticks out:
         if debug: 
            print(  'maxPos:', maxPos)
         if maxPos < len(goodImages)/2.:
           if debug:
             print( 'bad image in first half so add only images from second half' )
           for i in range(maxPos,len(goodImages)):
              newSciFileNames2.append(goodImages[i])
         else:
           if debug:
              print( 'bad image in second half so add only images from first half')
           for i in range(0,maxPos):
              newSciFileNames2.append(goodImages[i])
         printL('timings not OK, list downselected to '+str(newSciFileNames2) +'\n',debug)  
      else:
         printL('timings OK'+'\n',debug)  
         newSciFileNames2 =  goodImages
      if debug:
        print( 'newSciFileNames2:', newSciFileNames2)
      

      ## Check offset position are the same:

      printL('\n' + 'Check only A or B offset are selected: ' + '\n',debug)

      newSciFileNamesTmp = [] 
      nodA = ''
      nodB = ''

      if debug:
        print( 'filelist at start: ' + str(newSciFileNames2))

      for entry in newSciFileNames2:
        checkPoff = round(pOffsets[entry],2)
        checkQoff = round(qOffsets[entry],2)
        if debug:
          print( entry, 'checkPoff, checkQoff:', checkPoff, checkQoff)
        if nodA == '':
            nodA = (checkPoff,checkQoff)
            printL('nodA defined: '+ str(nodA),debug)
            newSciFileNamesTmp.append(entry)
        else:
            if nodB == '':
              if cmp((checkPoff,checkQoff),nodA) != 0:
                 # next image is new B pos
                 nodB = (checkPoff,checkQoff)
                 printL('nodB defined: '+ str(nodB) + '\n',debug)
                 newSciFileNamesTmp.append(entry)
              else:
                 #next image is again A pos
                 newSciFileNamesTmp.append(entry)
            else:
              if cmp((checkPoff,checkQoff),nodA) != 0 and cmp((checkPoff,checkQoff),nodB) != 0:
                #print 'exclude', (checkPoff,checkQoff)
                printL('exclude ' + entry + '\n', debug)
              else:
                newSciFileNamesTmp.append(entry)
      newSciFileNames2 = newSciFileNamesTmp
      if debug:
        print( 'filelist at end: ' + str(newSciFileNames2))


      ## Check  that each 'image' has still at least one 'sky':
     
      printL('\n' + 'Check  that each "image" has still at least one "sky": ' + '\n',debug)
       
      qOffsetsNew = {}
      for entry in newSciFileNames2:
        qOffsetsNew[entry] = qOffsets[entry]
      goodImages = checkForSky(qOffsetsNew,debug) 

      if len(goodImages) == 0:
            if debug:
              print( 'No skies in Q, so looking for P')
            goodImages = checkForSky(pOffsets,debug) 
 
      if goodImages != sciFileNames:
         logString = 'Replaced original files list ' +'\n'+ str(sciFileNames) +'\n' + ' with modified one ' +'\n'+ str(goodImages) 
         printL(logString,debug)
         # Re-check for best acquisition if first science file changed:
         if goodImages[0] != sciFileNames[0]:
            logString = '\n'+'First science image in list has changed, so looking again for best acquisition (i.e. to catch re-acquisition cases):'
            printL(logString,debug)
            adSci= AstroData(goodImages[0])
            obsid = adSci.phu_get_key_value("OBSID")
            progId = getProgId(obsid)
            if acqFilePos == -2: 
               obsidAcqFiles  = get_acq('acq', goodImages[0], dateOfNight, progId, debug)
            else: 
               obsidAcqFiles  = get_acq('acqCal', goodImages[0], dateOfNight, progId, debug)
            if debug:
              print( obsidAcqFiles)
            oldAcqFile= validObsidsInfo[validObsid][acqFilePos][0]
            #Replace acqfile:
            validObsidsInfo[validObsid][acqFilePos] = obsidAcqFiles
            #Remove old acqFile:
            if oldAcqFile in glob.glob(tempDir+'/*.fits'):
               os.remove(tempDir+oldAcqFile)
            #Download new:
            if obsidAcqFiles[0] not in glob.glob(tempDir+'/*.fits'):
               fetch_files(obsidAcqFiles[0],debug)
         validObsidsInfo[validObsid][checkFilesPos] = goodImages
      #print goodImages

      return validObsidsInfo  

#--------
def checkCoaddsTexps(validObsidsInfo, tempDir, dateOfNight,debug):
   for validObsid in validObsidsInfo:
      logString = '======================================================'+ '\n'
      logString = logString + 'Checking ' + validObsid + ' to make sure there all images have the same #coadds/Texps' + '\n'

      logString = logString + ' .... in science .... ' + '\n'
      printL(logString,debug)

      validObsidsInfo = checkTiming(validObsidsInfo, validObsid, 0, tempDir, dateOfNight,debug)

   return validObsidsInfo  

#--------
def checkVarSkies(validObsidsInfo, tempDir, dateOfNight,debug):
   for validObsid in validObsidsInfo:
      logString = '======================================================'+ '\n'
      logString = logString + 'Checking ' + validObsid + ' to make sure there are no variably spaced skies' + '\n'

      logString = logString + ' .... in science .... ' + '\n'
      printL(logString,debug)
      # Iterate 5 times: 
      for i in range(5):
         #sci acq is -2 and sci files is 0 in list 
         validObsidsInfo = checkSkies(validObsidsInfo, validObsid, -2, 0, tempDir, dateOfNight,debug)

      logString = '\n' + ' .... in telluric .... ' + '\n'
      printL(logString,debug)
      #tell acq is -1 and tell files is -3 in list
      validObsidsInfo = checkSkies(validObsidsInfo, validObsid, -1, -3, tempDir,dateOfNight,debug)

   return validObsidsInfo  


#--------
def fixHeader(validObsidsInfo,obsidsInfo, tempDir, debug):
  for validObsid in validObsidsInfo:
      logString = 'Checking ' + validObsid + ' to make sure OBJECT headers match' + '\n'
      logString =  logString + str(obsidsInfo[validObsid][-2]) + '\n'
      printL(logString,debug) 
      fileName = tempDir+obsidsInfo[validObsid][-2][0]
      ad= AstroData(fileName)
      objectName = ad.phu_get_key_value("OBJECT")  
      logString = logString + objectName + allTargetNames[validObsid]
      printL(logString,debug)
      if objectName != allTargetNames[validObsid]:
        logString = 'acq file object name changed !' + '\n'
        printL(logString,debug)
        ad.phu_set_key_value("OBJECT",allTargetNames[validObsid])
        ad.write(fileName)

#--------
def getProgId(obsid):
      #Only works for Q, LP, FT, and DD programs not for ENG or CAL programs 
      obsidSplit = obsid.split('-')
      progId = ""
      for i in range(0,3):
         #print i, obsidSplit[i]
         progId = progId + obsidSplit[i] + "-"
      progId = progId + obsidSplit[3]  
      return progId

#--------
def get_filelists(dateOfNight, obsids, obsidsFileList,  debug):
   obsidsInfo = {}       
   ## Get filelists for each obsid:  
   for obsid in obsids:
      logString = '======================================================'+ '\n'
      printL(logString, debug)
      logString = 'Looking for ' + obsid + '\n'
      printL(logString, debug)
      obsidSciFiles = sorted(obsidsFileList[obsid])
      if len(obsidSciFiles) > 1:
        firstScience = obsidSciFiles[1]
      else:
        firstScience = obsidSciFiles[0]
      logString = 'second (or only) science frame ' + firstScience + '\n'
      printL(logString,debug)
      progId = getProgId(obsid)
      obsidAcqFiles  = get_acq('acq', firstScience, dateOfNight, progId, debug)
      obsidArcFiles, junk = get_cals('arc',firstScience,obsid,debug)
      obsidFlatFiles1, junk = get_cals('flat',firstScience,obsid,debug)
      obsidFlatFiles2, junk = get_cals('qh_flat',firstScience,obsid,debug)
      obsidFlatFiles = obsidFlatFiles1 + obsidFlatFiles2
      obsidPinFiles, junk = get_cals('pinhole_mask',firstScience,obsid,debug)
      obsidTelFiles, progIdTel = get_cals('telluric_standard',firstScience,obsid,debug)
      if len(obsidTelFiles) == 0:
         obsidAcqCalFiles = []
      else:
         firstTelluric = obsidTelFiles[0]
         obsidAcqCalFiles= get_acq('acqCal', firstTelluric, dateOfNight, progIdTel, debug)
      obsidSciFiles.sort()
      obsidArcFiles.sort()
      obsidFlatFiles.sort()
      obsidPinFiles.sort()
      obsidTelFiles.sort()
      obsidAcqFiles.sort()
      obsidAcqCalFiles.sort()
      logString = ''+'\n'
      logString = logString + 'SciFiles: ' + str(obsidSciFiles) + '\n'
      logString = logString + 'ArcFiles: ' + str(obsidArcFiles) + '\n'
      logString = logString + 'FlatFiles: ' + str(obsidFlatFiles) + '\n'
      logString = logString + 'PinFiles: ' + str(obsidPinFiles) + '\n'
      logString = logString + 'TelFiles: ' + str(obsidTelFiles) + '\n'
      logString = logString + 'AcqFile: ' + str(obsidAcqFiles) + '\n'
      logString = logString + 'AcqCalFile: ' + str(obsidAcqCalFiles) + '\n'
      printL(logString, debug)

      obsidsInfo[obsid] = [obsidSciFiles,obsidArcFiles,obsidFlatFiles,obsidPinFiles,obsidTelFiles,obsidAcqFiles, obsidAcqCalFiles]

   return obsidsInfo


#--------
def move_temp(dateOfNight, tempDir, allDataDir, timeString, debug):
   if debug:
     logString = '\n' + 'Copy temp/ dir to allData (debug, so not executing it!)' + '\n' 
   else:
     logString = '\n' + 'Copy temp/ dir to allData' + '\n' 
   printL(logString,debug)
   newDir = allDataDir+dateOfNight+'/'
   if len(glob.glob(newDir)) == 0:
     if debug: 
       pass
     else:
       shutil.copytree(tempDir,newDir,symlinks=True)
   else:
     logString = newDir + ' already exists in allData, so will check subdirs'+ '\n' 
     printL(logString,debug)
     obsidDirs = glob.glob(tempDir+'/*')
     for obsidDirFull in obsidDirs:
       obsidDir = obsidDirFull.split('/')[-1]
       addDir = newDir+obsidDir+'/'
       if len(glob.glob(addDir)) == 0:
          logString = addDir + ' does not exist, so will copy data from ' +tempDir+obsidDir+'/ over'+ '\n' 
          printL(logString,debug)
          if debug:
             print( 'Would be copying data from here (' +  tempDir+obsidDir+'/) to here ('+  addDir+')')
          else: 
             shutil.copytree(tempDir+obsidDir+'/',addDir,symlinks=True)
       else:
          addDirBak = addDir[:-1]+'_old_'+timeString+'/'
          logString = addDir + ' does already exist, so will create backup ('+addDirBak+'), delete old version, and copy data from '+ tempDir+obsidDir+'/ over.' + '\n' 
          printL(logString,debug)
          if debug:
            print( 'Would be copying data from here ('+addDir+') to here ('+  addDirBak+')')
            print( 'Would be removing '+addDir)
            print( 'Would be copying data from here (' +  tempDir+obsidDir+'/) to here ('+  addDir+')')
          else:
            shutil.copytree(addDir,addDirBak,symlinks=True)
            shutil.rmtree(addDir)
            shutil.copytree(tempDir+obsidDir+'/',addDir,symlinks=True)


#--------
# Copy result files to webserver: 
def copy_webpage(webUpdateList, webDir, resultDir, debug):
    if debug:
      logString = 'Copy result files to webserver (only print cmd, but not executing it!)' + '\n' 
    else:
      logString = 'Copy result files to webserver' + '\n' 
    printL(logString,debug)
    for entry in webUpdateList:
        obsid = entry[0]
        dateOfNight = entry[1]
        targetName = obsid+'_'+entry[2]        
        dataFiles= [targetName+'.txt',targetName+'.fits',targetName+'_data_sheet.pdf' ]
        logString = ''
        for eachFile in dataFiles:
           cmd = 'scp -B ' +resultDir+eachFile + ' gnda@hbfweb09:' + webDir
           logString = logString + cmd + '\n' 
           printL(logString,debug)
           if debug:
            pass
           else:
             pass
             #os.system(cmd)
        #Copy webpage over: 
        cmd = 'scp -B ' +resultDir + 'gnirsPipeOutput.ihtml' + ' gnda@hbfweb09:' + webDir
        logString = logString + cmd + '\n'
        printL(logString,debug)
        if debug:
           pass
        else:
           pass
           #os.system(cmd)      

#--------
def copy_results(webUpdateList, tempDir, dataDir, resultDir, debug):
    if debug:
      logString = 'Copy result files to allResults (only print cmd, but not executing it!)' + '\n' 
    else:
      logString = 'Copy result files to allResults' + '\n' 
    printL(logString,debug)
    for entry in webUpdateList:
        obsid = entry[0]
        dateOfNight = entry[1]
        targetName = entry[2]
        dataPath =  tempDir+obsid+dataDir
        dataFiles= [targetName+'.txt',targetName+'.fits',targetName+'_data_sheet.pdf' ]
        for eachFile in dataFiles:  
           logString = 'shutil.copy(' + str(dataPath) + str(eachFile)+ ','+ str(resultDir)+ str(obsid)+'_'+str(eachFile)+')'
           printL(logString,debug)
           if debug:
             pass
           else:
             shutil.copy(dataPath+eachFile, resultDir+obsid+'_'+eachFile)

#--------
def update_webpage(webUpdateList, timeString, resultDir,debug):
    #
    logString = 'Update webpage ' + '\n' 
    printL(logString, debug)
    #
    htmlFile = resultDir+'gnirsPipeOutput.ihtml'
    #
    #Make backup: 
    if debug:
      htmlFileBackup = htmlFile[:-6].replace('gnirsPipeOutput','debug_gnirsPipeOutput')+'_'+timeString+'.ihtml'
    else:
      htmlFileBackup = htmlFile[:-6]+'_'+timeString+'.ihtml'
    shutil.copy(htmlFile,htmlFileBackup)
    # 
    sortLines = []
    #
    for line in open(htmlFile):
        if "UPDATE" in line: 
          sortLines.append(line)

    #
    for entry in webUpdateList:
        obsid = entry[0]
        dateOfNight = entry[1]
        targetName = obsid+'_'+entry[2]
        #newHtmlEntry = '<!-- UPDATE '+ dateOfNight+ ' --><td align="center">'+dateOfNight+'</td><td align="center">'+obsid+'</td><td align="center"> <a href="http://www.gemini.edu/sciops/instruments/gnirs/pipeOutput/'+targetName+'.txt">txt</a> <a href="http://www.gemini.edu/sciops/instruments/gnirs/pipeOutput/'+targetName+'.fits">fits</a> </td> <td align="center"><a href="http://www.gemini.edu/sciops/instruments/gnirs/pipeOutput/'+targetName+'_data_sheet.pdf">pdf</a> </td></tr>'+'\n'
        # --> replaced with local link for now
        newHtmlEntry = '<!-- UPDATE '+ dateOfNight+ ' --><td align="center">'+dateOfNight+'</td><td align="center">'+obsid+'</td><td align="center"> <a href="file:///home/gnda/gnirsPipe/allResults/'+targetName+'.txt">txt</a> <a href="file:///home/gnda/gnirsPipe/allResults/'+targetName+'.fits">fits</a> </td> <td align="center"><a href="file:///home/gnda/gnirsPipe/allResults/'+targetName+'_data_sheet.pdf">pdf</a> </td></tr>'+'\n'

        if newHtmlEntry not in sortLines: 
           sortLines.append(newHtmlEntry)
        else:
           logString = 'There is already an entry for '+obsid+' on the webpage, skipping addition of newline. Updated files are copied to allResults though. ' + '\n' 
           printL(logString, debug)
       
    ## Sort by date, newest first: 
    sortLines.sort()
    sortLines.reverse() 
    #
    ## Overwrite existing webpage with new version:
    if debug:   
       htmlFileNew = open(htmlFile.replace('gnirsPipeOutput','debug_gnirsPipeOutput'), 'w')
    else:
       htmlFileNew = open(htmlFile, 'w')
    #
    for line in open(htmlFileBackup):
       if "UPDATE" not in line:
          htmlFileNew.write(line)
       if "TOP" in line:
          for entry in sortLines:
             htmlFileNew.write(entry+'\n')
    #
    htmlFileNew.close()

#--------
def sendMail (toaddress,fromaddress,subject,message):
    ## GN-specific!
    smtpserver = smtplib.SMTP("smtp.hi.gemini.edu")
    toaddressString = ', '.join(toaddress)
    header = 'To:' + toaddressString + '\n' + 'From:' + fromaddress + '\n' + 'Subject: ' + subject +'\n'
    msg = header + '\n' + message
    smtpserver.sendmail(fromaddress, toaddress, msg)
    smtpserver.close()

#--------
## Select valid obsids and get all fits files: 
def checkFileLists (obsidsInfo,debug):
      validObsidsInfo = {}

      for entry in obsidsInfo:
        #print 'entry ', entry
        obsid = entry
        logString ='======================================================'+    '\n'    
        logString = logString + str(obsid)+    '\n'    
        printL(logString, debug)

        # Check minimum number of files needed to included science:
        min_sci = 4
        min_arc = 1
        min_flat = 5
        min_pin = 1
        min_tel = 4
        min_acq = 1
        min_acqCal = 1

        if len(obsidsInfo[entry][0]) >= min_sci and len(obsidsInfo[entry][1]) >= min_arc and len(obsidsInfo[entry][2]) >= min_flat and len(obsidsInfo[entry][3])>min_pin and len(obsidsInfo[entry][4]) >= min_tel and len(obsidsInfo[entry][5]) >= min_acq and len(obsidsInfo[entry][6]) >= min_acqCal   : 

          validObsidsInfo[entry] = obsidsInfo[entry] 
          logString = obsid+' does match minimum number criterion' + '\n' 
          logString =  logString + '#sci: ' + str(len(obsidsInfo[entry][0])) + '\n' 
          logString =  logString + '#arc: ' + str(len(obsidsInfo[entry][1])) + '\n' 
          logString =  logString + '#flat: ' + str(len(obsidsInfo[entry][2])) + '\n' 
          logString =  logString + '#pin: ' + str(len(obsidsInfo[entry][3])) + '\n' 
          logString =  logString + '#tell: ' + str(len(obsidsInfo[entry][4])) + '\n' 
          logString =  logString + '#acq: ' + str(len(obsidsInfo[entry][5])) + '\n' 
          logString =  logString + '#acqCal: ' + str(len(obsidsInfo[entry][6])) + '\n' 
          printL(logString, debug)


        else:
          logString = obsid+' does not match minimum number criterion' + '\n' 
          
          logString =  logString + '#sci: ' + str(len(obsidsInfo[entry][0])) + '\n' 
          logString =  logString + '#arc: ' + str(len(obsidsInfo[entry][1])) + '\n' 
          logString =  logString + '#flat: ' + str(len(obsidsInfo[entry][2])) + '\n' 
          logString =  logString + '#pin: ' + str(len(obsidsInfo[entry][3])) + '\n' 
          logString =  logString + '#tell: ' + str(len(obsidsInfo[entry][4])) + '\n' 
          logString =  logString + '#acq: ' + str(len(obsidsInfo[entry][5])) + '\n' 
          logString =  logString + '#acqCal: ' + str(len(obsidsInfo[entry][6])) + '\n' 
          printL(logString, debug)

      return validObsidsInfo


#--------
## Select valid obsids and get all fits files: 
def valid_fetch (obsidsInfo,debug):

      validObsidsInfo = checkFileLists (obsidsInfo,debug)

      for entry in validObsidsInfo:
          obsid = entry
          logString ='======================================================'+    '\n'    
          logString = logString + str(obsid)+    '\n'    
          printL(logString, debug)


 
          logString ='files will be fetched for ' + obsid +    '\n'     
          printL(logString, debug)

          ## Download fits files: 
          for fileList in obsidsInfo[entry]:
             for myFile in fileList:
               fetched = fetch_files(myFile,debug)


      return validObsidsInfo

#--------
## Create directories for selected obsids and copy files :
def create_copy (validObsidsInfo,tempDir,codeVersion,codeOld,codeNew, debug):

      ## Loop over all valid obsids:  
      for entry in validObsidsInfo:
         obsid = entry
         logString = '======================================================'+ '\n'
         printL(logString,debug)
         # Create dir for each obsid
         if len(glob.glob(tempDir+'/'+obsid+'/')) == 0:
           logString = 'Creating a directory for ' + obsid + '\n'
           printL(logString,debug)
           os.mkdir(obsid)
           os.mkdir(obsid+"/raw") #create raw directory where all data goes
         else:
           logString = obsid+"/raw"+' already exists'+ '\n'
           printL(logString,debug)
         path = obsid+"/raw"
         filelist = open(obsid+'/inputfiles.lst', 'a') # open inputfiles.lst
         for fileList in validObsidsInfo[entry]:
               for myFile in fileList:
                 #print tempDir+'/raw/'+myFile
                 if len(glob.glob(tempDir+obsid+'/raw/'+myFile)) == 0:
                   shutil.copy(myFile, path)
                   filelist.write('%s\n' % myFile)
         filelist.close()            

         ## Create std_star.txt to run pipeline version v20a/b (temp. fix)
         if codeVersion in codeOld:
            stdDir= '/OUTPUTS/'
         elif codeVersion in codeNew: 
            stdDir= '/INTERMEDIATE/'
         if (codeVersion in codeOld) or (codeVersion in codeNew):
           logString = 'Creating mock std_star.txt' + '\n'
           if len(glob.glob(tempDir+obsid+stdDir)) == 0: 
             os.mkdir(tempDir+obsid+stdDir)
           if len(glob.glob(tempDir+obsid+stdDir+"std_star.txt")) == 0:
             logString = logString + 'file will be in ' + tempDir+obsid+stdDir+"std_star.txt " + '\n'
             f = open(tempDir+obsid+stdDir+"std_star.txt", 'w')
             f.write("k K N/A 9700 "+ '\n') 
             f.write("h H N/A 9700 "+ '\n') 
             f.write("j J N/A 9700 "+ '\n')
             f.write("j J N/A 9700 "+ '\n')
             f.write("i I N/A 9700 "+ '\n')
             f.write("i I N/A 9700 "+ '\n') 
             f.close()
         printL(logString,debug)

#--------
def get_acq(acqType, filename, dateOfNight, progid, debug):

   url = "jsonfilelist/%s/%s/%s/GNIRS/present" % (progid, dateOfNight,acqType) 

   filesFull = get_info(url,debug)
   files = []

   acqFilename = select_closest(filename, filesFull, debug)

   ## Catch cases where acquisition is wrongly labelled: 
   if int(acqFilename[-9:-5]) == 0:
     logString = 'No ' +acqType+ ' found, wrongly labelled acquisition?' + '\n' +  '\n' 
     printL(logString, debug)
     if acqType == 'acq': 
        acqType = 'acqCal'
     if acqType == 'acqCal': 
        acqType = 'acq'
     url = "jsonfilelist/%s/%s/%s/GNIRS/present" % (progid, dateOfNight,acqType)
     filesFull  = get_info(url,debug)
     files = []
     acqFilename = select_closest(filename, filesFull, debug)

   returnList =  [acqFilename]

   if int(acqFilename[-9:-5]) == 0:
     returnList = []

   return returnList

#--------
def select_closest(filename, filesFull, debug):

   sciFileNum = int(filename[-9:-5])
 
   bestFileNum = 0
   lastNum = 0
   for entry in filesFull:
      acqFileNum = int(str(entry['name'])[-9:-5])
      if acqFileNum > sciFileNum:
        bestFileNum = lastNum
        break 
      else:
        lastNum = acqFileNum
   if bestFileNum == 0:
      bestFileNum = lastNum

   acqFilename = '%s%04i.fits' % (filename[:-9],bestFileNum) 
 
   logString = 'acq./acqCal Filename: ' + acqFilename + '\n'
   printL(logString, debug)

   return acqFilename


#--------
def get_cals(calType, filename, obsid, debug):
  
   url = "calmgr/%s/%s/present" % (calType, filename)

   filesFull = get_info(url,debug)
  
   files = []

   # Catch cases where cal file is missing: 
   progIdTel = ''
   if len(filesFull) == 0 : 
     logString = 'No machting '+calType+ ' found!' + '\n'
     printL(logString, debug)
   else:
     #Append cal files to list:
     for entry in filesFull:
       # for flat only include flats with same datalabel: 
       if calType == 'flat':
         if entry['datalabel'][:-4] == obsid:
            files.append(entry['name'])
       # for arcs select second one if available:
       if calType == 'arc':
            arcUrl = "jsonfilelist/%s/ARC/present"  % (obsid)
            arcFiles = get_info(arcUrl,debug)
            if len(arcFiles) > 1:
              logString = 'Selected last arc file in sequence ('+arcFiles[-1]['name']+') instead of ' + entry['name'] + '\n'
              printL(logString, debug)
              files.append(arcFiles[-1]['name'])
            else:
              files.append(entry['name'])
       else:
         files.append(entry['name'])

     #Rerun for flats to catch on sky flats taken in different obsid:
     if calType == 'flat':
       if len(files) == 0 : 
          for entry in filesFull:
            files.append(entry['name'])

     #Rerun for telluric to select only one obsid, i.e. the closest one

     if calType == 'telluric_standard':
       firstEntry = files[0]
       refObsid = ""
       selectedTellurics = []
       for entry in filesFull:
          if entry['name'] == firstEntry:
            refObsid = entry['datalabel'][:-4]
            selectedTellurics.append(firstEntry)
          else:
            checkObsid = entry['datalabel'][:-4]
            if  checkObsid == refObsid: 
              selectedTellurics.append(entry['name'])
       progIdTel = getProgId(refObsid)
       selectedTellurics = sorted(selectedTellurics)
       logString = 'selectedTellurics: ' + str(selectedTellurics) + ' from program '+progIdTel +'\n'
       printL(logString, debug)
       files = selectedTellurics
  
   return files, progIdTel

#--------
# Return Obs-IDs for a given night with a particular disperser
def get_obsids(disperser, dateOfNight, obsidRun, debug):
   printL('obsidRun = ' + obsidRun, debug)
   url = "jsonsummary/%s/science/GNIRS/disperser=%s/Lucky/present" % (dateOfNight, disperser)

   files = get_info(url, debug)

   logString =  'number of science frames: ' + str(len(files)) + '\n' 
   printL(logString,debug)

   obsids = []
   targetNames = {}
   obsidsFileList = {} 
   ignore = False 
   for myFile in files: 
     newObsid = str(myFile['observation_id'])
     fileName = str(myFile['name'])
     targetName = str(myFile['object']).replace(' ','')
     if debug:
       print( fileName, myFile['central_wavelength'])
     if myFile['central_wavelength'] != None:
       centrWave = round(myFile['central_wavelength'],2)
     else:
       printL('Central wavelength not defined for '+fileName+', please check if image OK',debug )
       centrWave = 999

     if newObsid in obsids:
       obsidsFileList[newObsid].append(fileName)
     else:
       addObsid = False
       if newObsid == obsidRun:
         addObsid = True         
       if obsidRun == '':
         #Make sure no ENG program is included: 
         if 'ENG' not in newObsid:
            addObsid = True 
         else:
            ignore = True
                
         #Exclude all non 1.65mu central wavelength for 32 l/mm:
         if disperser == '32lXD' and  centrWave != 1.65:
            addObsid = False
            logString =  fileName + ' (obsid: ' + str(newObsid) + ') at non-standard 32 l/mm central wavelength 1.65: ' +str(centrWave)
            printL(logString,debug)
       if addObsid:
         obsids.append(newObsid)
         targetNames[newObsid] = targetName
         obsidsFileList[newObsid] = [fileName] 
   # Print identified ENG program: 
   if ignore:
      logString = '\n'+ newObsid + ' is an ENG program, so will be ignored'
      printL(logString,debug) 

   logString =  '\n'+ 'obsids: ' + str(obsids) + '\n' 
   logString = logString + 'targetNames: ' + str(targetNames) + '\n' 
   printL(logString,debug)
   return obsids, obsidsFileList, targetNames


#--------
# Query FITS or the Gemini Archive and return a list of files
def get_info(urlInput, debug):
   printL('urlInput = ' + urlInput, debug)
   server = 'http://fits'

   url = "%s/%s" % (server,urlInput)

   logString = 'querying ' + '\n'
   logString = logString + str(url) + '\n'
   printL(logString,debug)
   
   files = read_url(url,debug)

   if len(files) > 0:
     pass
   else:
      logString = 'Query unsuccessful on "fits" server, trying archive now' + '\n'
      printL(logString,debug)
      server = 'https://archive.gemini.edu'
#      if obsidRun:
#         url = "%s/jsonfilelist/%s/acq/GNIRS" % (server,obsid)
#      else:
      url = "%s/%s" % (server,urlInput)
      try:
        logString =  'querying ' + str(url) + '\n'
        printL(logString,debug)
        files = read_url(url,debug)
      except:
        logString = 'Cannot connect to archive...maybe its down? Please check. ' + '\n'
        printL(logString,debug)

   return files


#--------
def read_url(url,debug):

   u = urllib2.urlopen(url)

   if 'json' in url:
     if debug:
       logString = 'json url'
       printL(logString,debug)
     jsontext = u.read()
     files = json.loads(jsontext)
     u.close()
   else:
     if debug:
       logString = 'xml url'
       printL(logString,debug)
     xml = u.read()
     u.close()

     dom = parseString(xml)
     files = []
     for fe in dom.getElementsByTagName("calibration"):
                dict = {}
                dict['name']=str(fe.getElementsByTagName("filename")[0].childNodes[0].data)
                dict['datalabel']=str(fe.getElementsByTagName("datalabel")[0].childNodes[0].data)
                dict['caltype']=str(fe.getElementsByTagName("caltype")[0].childNodes[0].data)
                dict['md5']=str(fe.getElementsByTagName("md5")[0].childNodes[0].data)
                dict['url']=str(fe.getElementsByTagName("url")[0].childNodes[0].data)
                #if debug:
                #  print dict
                files.append(dict)
   #if debug:
   # print 'read_url files ' , files
   return files 

#--------
def get_file_list(disperser, dateOfNight, debug):

   url = "jsonfilelist/summary/%s/gnirs/disperser=%s/Lucky/present" % (dateOfNight, disperser)

   files = get_info(url, debug)

   return files


#--------
def fetch_files(myFile, debug):

   fetched = True

   matches = []
   for root, dirnames, filenames in os.walk(os.getcwd()):
     #print filenames
     for filename in fnmatch.filter(filenames, myFile):
       matches.append(os.path.join(root, filename))

   logString = 'Search for file ' + myFile + ' on disk: ' + str(matches) + '\n' 
   printL(logString,debug)

 
   if len(matches) == 0 :

    server='http://fits'
    url = "%s/file/%s" % (server, myFile)
    opener = urllib2.build_opener()
    opener.addheaders.append(('Cookie', 'gemini_fits_authorization=good_to_go'))

    try:
      logString = "Fetching " + str(url) + '\n'
      printL(logString,debug)
      u = opener.open(url)   
      data = u.read()       
      u.close()
    except:
      logString = 'File not available on "fits" server trying archive' + '\n' 
      printL(logString,debug)
      server = 'https://archive.gemini.edu'
      url = "%s/file/%s" % (server, myFile)
      logString = "Fetching " + str(url) + '\n' 
      printL(logString,debug)
      u = opener.open(url)   
      data = u.read()       
      u.close()
   

### not working at the moment, might be rewritten and reused at some point
#    # Compute the md5
#    m = hashlib.md5()
#    m.update(data)
#    md5 = m.hexdigest()
#    if debug:
#      print 'database md5: ' ,myFile['data_md5']
#      print 'fetched file md5: ', md5        
#    # Compare against database value
#    # If OKAY, write file, if not, complain
#    if(md5==myFile['data_md5']):
#       print "fetched file %s" % myFile['name']
#       f = open(myFile['name'], 'w')
#       f.write(data)
#       f.close()
#    else:
#       print "md5 did not match, problem with file %s" % myFile['filename']

    logString = "fetched file " + str(myFile) + '\n' 
    printL(logString,debug)
    f = open(myFile, 'w')
    f.write(data)
    f.close()

   else:
      logString = "file already on disk, so fetching skipped " + '\n' 
      printL(logString,debug)
      fetched = False
  
   return fetched

#--------
def printL(logString, debug):
   if debug:
     #print ""
     print( logString)
   logFile.write('\n')
   logFile.write(logString)


############################################################################
############################################################################

if __name__ == '__main__':

#    parser = argparse.ArgumentParser(
#        description='Reduce GNIRS XD data of a utdate',
#        epilog='Version: ' + version,
#        version=version)
#    # Required arguments:
#    parser.add_argument('XML', help='XML files')
    # Boolean options:
#    parser.add_argument('-d', '--debug',  action='store_true',
#default=False, help='Show debugging output')
 #   # Options requiring values:
 #   #parser.add_argument('-r', '--radius', action='store', type=float,default=0.117, help='Search radius (degrees) [0.117]')
#    args = parser.parse_args()

   ## Define the command line options

   usage ="""
   Program does .......   

   %prog [options] <dateOfNight>

   DateOfNight (YYYYMMDD) is optional, if no date is given, script will take the current (local) date. In Hilo the date after the night == UT date, as needed. 

   """

   p = OptionParser(usage=usage)

   p.add_option('-d', '--debug', action='store_true',
             dest='debug', default=False,
             help='(optional) Print additional debug lines')

   p.add_option('-r', '--runPipe', action='store_false',
             dest='runPipe', default=True,
             help='(optional) Switch off actual pipeline run. ')

   p.add_option('-o', '--obsidRun', action='store',
             dest='obsidRun', default='',
             help='(optional) Run code for specific OBSID. ')

   p.add_option('-c', '--codeVersion', action='store_false',
             dest='codeVersion', default='v226',
             help='(optional) Change of GNIRS pipeline code')

   p.add_option('-p', '--disperser', dest='dispList', action='store', default = "32lXD", help="(optional) Restrict to one dispersers, e.g. -p '32lXD'")

   p.add_option('-l', '--local', action='store_true',
               dest='localRun', default=False,
               help='Local run == printout in terminal without email sending (optional)')

   p.add_option('-e', '--email', action='store',
             dest='email', default=['jen.miller@noirlab.edu',],
             help='Provide email address for testing (optional)')


   ## Check and assign input:
   (options, args) = p.parse_args()

   main(options, args)


