#Test module - try to find edge of slit so can define aperture centre at expected Q offset position

#Input: science target data file, target_comb
#       (shouldn't be any need to do this for standard stars)

#Function: Find the left-hand end of the slit, and then the x pixel where the target spectrum should be centred
#          Doing this because it's not clear if hardwiring the position of the slit will be safe
#          But also not sure if this mean counts criterion is reliable...

#Output: column where science target spectrum should be, for use by nsextract

import re
from pyraf import *
#iraf.reset(gemini="extern$gemini_public_1_11_1/")
iraf.gemini(_doprint=0, motd="no")
iraf.gnirs(_doprint=0)
iraf.nsheaders('gnirs',Stdout='/dev/null')


testdir = '/Users/rmason/Observatory/GNIRS/autoredux/GN-2015A-DD-10-167/20111124_v22_beta/INTERMEDIATE/'


def find_edge(ext):
    x = -1
    leftside = True
    rightside = True
    counts = []
    while leftside:
        x += 1
        mean = (iraf.imstatistic(images=testdir+'target_comb[SCI,'+str(ext)+']['+str(x+1)+':'+str(x+1)+',700:800]',fields='mean',lower="INDEF",upper="INDEF",nclip=0,lsigma=3.0,usigma=3.0,binwidth=0.1,format='yes',cache='no',mode='al',Stdout=1))
        mean = float(mean[1])
        counts.append(mean)
        if x > 0 and counts[x] == counts[x-1]:
            continue
        elif x > 0 and counts[x] != counts[x-1]:
            leftedge = x+1
            print( 'LHS of slit in extension '+str(ext)+' is at x=', leftedge)
            leftside = False
    while rightside:
        x += 1
        mean = (iraf.imstatistic(images=testdir+'target_comb[SCI,'+str(ext)+']['+str(x+1)+':'+str(x+1)+',700:800]',fields='mean',lower="INDEF",upper="INDEF",nclip=0,lsigma=3.0,usigma=3.0,binwidth=0.1,format='yes',cache='no',mode='al',Stdout=1))
        mean = float(mean[1])
        counts.append(mean)
        if x > 0 and counts[x] != counts[x-1]:
            continue
        elif x > 0 and counts[x] == counts[x-1]:
            rightedge = x+1
            print( 'RHS of slit in extension '+str(ext)+' is at x=', rightedge)
            rightside = False

    print( "Effective slit length = ", rightedge - leftedge)
    print( "Slit centre = ", leftedge + (rightedge - leftedge)/2)

    #Turns out this doesn't actually work - in e.g. ext5 the left-hand end of the slit is "cut off", so the length and therefore centre aren't accurate.

for i in range (1, 7):
    find_edge(i) 
    

        
