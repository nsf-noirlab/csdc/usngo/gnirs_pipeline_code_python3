#!/usr/bin/env python
#
##
##
##    SdistortAndWLcalibXD.py:           
##
##
#
# Import libraries
#


import sys
from pyraf import *
import pyfits
import numpy
from pyfits import getdata, getheader
#iraf.reset(gemini="extern$gemini_public_1_11_1/")
iraf.gemini(_doprint=0, motd="no")
iraf.gnirs(_doprint=0)
iraf.imutil(_doprint=0)
iraf.nsheaders('gnirs',Stdout='/dev/null')
gemiraf = sys.argv[8:9][0]
path_to_tasks = sys.argv[9:10][0]
if gemiraf == "1.11.1":
    #Use the version of nssdist.cl that comes as standard with 1.12beta
    #Version is more reliable, helps avoid "snake effect" in transformed data
    iraf.task (nssdist = path_to_tasks+'nssdist.cl')

#Until next package release (WHICH ONE??), need to use Emma's new versions of nsfitcoords, nstransform, nswavelength and nswhelper
iraf.task (nsfitcoords = path_to_tasks+'nsfitcoords.cl')
iraf.task (nstransform = path_to_tasks+'nstransform.cl')
iraf.task (nswavelength = path_to_tasks+'nswavelength.cl')
iraf.task (nswhelper = path_to_tasks+'nswhelper.cl')
#Also want custom version of nschelper that get offsets from header keywords not WCS - never finished this
#iraf.task (nschelper = path_to_tasks+'nschelper.cl')

#Interactive options
nssdistinter = sys.argv[5:6][0]
nswavinter = sys.argv[6:7][0]
nsfitinter = sys.argv[7:8][0]

infile = sys.argv[1:2]
infile = infile[0]
file = open(infile,'r')
lines = file.readlines()

#Find out whether these are long camera data
camera = iraf.hselect(lines[0].replace('\n','')+'[0]',fields="CAMERA",expr="yes",missing="INDEF",Stdout=1)
if 'Long' in camera[0]: 
    pinhole_file = 'gnirs$data/pinholes-long-dense-north.lis'
elif 'Short' in camera[0]:
    pinhole_file = 'gnirs$data/pinholes-short-dense-north.lis'
    #this used to be no! Need to figure out what has changed
else:
    print( '***WARNING: Unknown camera. Will do s-distortion correction as for short camera data, but are you sure everything is OK?')
    pinhole_file = 'gnirs$data/pinholes-short-dense-north.lis'

    
j = 1
listaA = listaB = listaC = ''
for line in lines:
    line = line.replace('\n','')
    aux = line.split('\t')
    fileName = str(aux[0])
    aux = fileName.split('.')
    fileName = str(aux[0])
    if j == 1:            
        listaA += 'rnc'+fileName
        namepinhole = 'rnc'+fileName
    else:
        listaA += ',rnc'+fileName
    j = int(j) +1
    #Run nssdist on pinholes
    iraf.nssdist(inimages='rnc'+fileName,outsuffix='_sdist',pixscale=1.0,dispaxis=1,database='',firstcoord=0.0,coordlist=pinhole_file,aptable=path_to_tasks+'config/apertures.fits',fl_inter=nssdistinter,fl_dbwrite='yes',section='default',nsum=30,ftype='emission',fwidth=10.0,cradius=10.0,threshold=1000.0,minsep=5.0,match=-6.0,function="legendre",order=5,sample='',niterate=3,low_reject=5.0,high_reject=5.0,grow=0.0,refit='yes',step=10,trace='no',nlost=0,aiddebug='',logfile='',verbose='no',debug='no',force='no',mode='al')

    #Check that the right number of pinholes has been identified
    for i in range (1,7):
        database_file = open ('database/idrnc'+fileName+'_SCI_'+str(i)+'_')
        #read up to the first occurrence of "features"
        for entry in database_file:
            allOK = True
            if "features" in entry:
                if 'Short' in camera[0] and entry.split()[1] != '6':
                    print( "***WARNING: expected 6 pinholes to be detected by nssdist, but found", entry.split()[1], "in extension", str(i), ". This can cause problems; check the transformed data files (ttf*fits) and look for inter-order offsets in the 'orders.pdf' file created in step 11 (combining orders).")
                    allOK = False
                if 'Long' in camera[0] and entry.split()[1] != '9':
                    print( "***WARNING: expected 9 pinholes to be detected by nssdist, but found", entry.split()[1], "in extension", str(i), ". This can cause problems; check the transformed data files (ttf*fits) and look for inter-order offsets in the 'orders.pdf' file created in step 11 (combining orders).")
                    allOK = False
        if allOK:  
            print( "***CHECK: Correct number of pinholes detected by nssdist in extension", str(i))


infile = sys.argv[2:3]
infile = infile[0]
file = open(infile,'r')
lines = file.readlines()
j = 1
listaA = listaB = listaC = ''
for line in lines:
    line = line.replace('\n','')
    aux = line.split('\t')
    fileName = str(aux[0])
    aux = fileName.split('.')
    fileName = str(aux[0])    
    if j == 1:            
        listaA += 'rnc'+fileName
    else:
        listaA += ',rnc'+fileName
    j = int(j) +1


if len(listaA) > 17:
    #means there is more than 1 arc, so:
    iraf.nscombine(inimages=listaA,tolerance=0.5,output='arc_comb.fits',output_suffi='comb',bpm='',dispaxis=1,pixscale=1.0,fl_cross='no',fl_keepshift='no',fl_shiftint='yes',interptype='linear',boundary='nearest',constant=0.0,combtype='average',rejtype="none",masktype="none",maskvalue=0.0,statsec='[*,*]',scale='none',zero='none',weight='none',lthreshold='INDEF',hthreshold='INDEF',nlow=1,nhigh=1,nkeep=0,mclip='yes',lsigma=5.0,hsigma=5.0,pclip=-0.5,grow=0.0,nrejfile='',fl_vardq='yes',fl_inter='no',logfile='',verbose='yes',debug='no',force='no',mode='al')
else:
    iraf.copy (input=listaA+".fits", output="arc_comb.fits", verbose="no")


#Now run nsfitcoords and nstransform on the arc, to spatially rectify it before running nswavelength
iraf.nsfitcoords(inimages='arc_comb.fits',outspectra='',outprefix='f',lamptrans='',sdisttrans=namepinhole,dispaxis=1,database='',fl_inter=nsfitinter,fl_align='no',function='chebyshev',lxorder=2,lyorder=4,sxorder=4,syorder=4,pixscale=1.0,logfile='',verbose='yes',debug='no',force='no',mode='al')
iraf.nstransform(inimages='farc_comb.fits',outspectra='',outprefix='t',dispaxis=1,database='',fl_stripe='no',interptype='poly3',xlog='no',ylog='no',pixscale=1.0,logfile='',verbose='yes',debug='no',mode='al')

#At this stage the orders should be vertical, but the arc lines are still a bit tilted because of the detector rotation
#Run nswavelength on the transformed arc with fl_median-, which calls nsfitcoords to produce an arc with horizontal lines
#iraf.nswavelength(lampspectra="tfarc_comb",outspectra='',outprefix='w',crval='INDEF',cdelt='INDEF',crpix='INDEF',dispaxis=2,database='',coordlist='gnirs$data/lowresargon.dat',fl_inter=nswavinter,nsappwavedb=path_to_tasks+'config/nsappwave.fits',fl_median="no",sdist='',sdorder=4,xorder=2,yorder=2,aptable=path_to_tasks+'config/apertures.fits',section='auto',nsum=10,ftype='emission',fwidth=4.0,cradius=5.0,threshold=300.0,minsep=2.0,match=-6.0,function='chebyshev',order=4,sample='*',niterate=10,low_reject=3.0,high_reject=3.0,grow=0.0,refit='yes',step=2,trace='no',nlost=10,fl_overwrite='yes',aiddebug='',fmatch=0.2,nfound=6,sigma=0.05,rms=0.1,logfile='',verbose='yes',debug='no',mode='al')
iraf.nswavelength(lampspectra="tfarc_comb",outspectra='',outprefix='w',crval='INDEF',cdelt='INDEF',crpix='INDEF',dispaxis=2,database='',coordlist='gnirs$data/lowresargon.dat',fl_inter="no",nsappwavedb=path_to_tasks+'config/nsappwave.fits',fl_median="no",sdist='',sdorder=4,xorder=2,yorder=2,aptable=path_to_tasks+'config/apertures.fits',section='auto',nsum=10,ftype='emission',fwidth=4.0,cradius=5.0,threshold=300.0,minsep=2.0,match=-6.0,function='chebyshev',order=4,sample='*',niterate=10,low_reject=3.0,high_reject=3.0,grow=0.0,refit='yes',step=2,trace='no',nlost=10,fl_overwrite='yes',aiddebug='',fmatch=0.2,nfound=6,sigma=0.05,rms=0.1,logfile='',verbose='yes',debug='no',mode='al')
#Run nsfitcoords and nstransform on the transformed arc using the output from nswavelength for the "lamp" info

iraf.nsfitcoords(inimages='tfarc_comb.fits',outspectra='',outprefix='f',lamptrans='wtfarc_comb.fits',sdisttrans='',dispaxis=1,database='',fl_inter=nsfitinter,fl_align='no',function='chebyshev',lxorder=2,lyorder=4,sxorder=4,syorder=4,pixscale=1.0,logfile='',verbose='yes',debug='no',force='no',mode='al')
#iraf.nsfitcoords(inimages='tfarc_comb.fits',outspectra='',outprefix='f',lamptrans='wtfarc_comb.fits',sdisttrans='',dispaxis=1,database='',fl_inter="yes",fl_align='no',function='chebyshev',lxorder=2,lyorder=4,sxorder=4,syorder=4,pixscale=1.0,logfile='',verbose='yes',debug='no',force='no',mode='al')
iraf.nstransform(inimages='ftfarc_comb.fits',outspectra='',outprefix='t',dispaxis=1,database='',fl_stripe='no',interptype='poly3',xlog='no',ylog='no',pixscale=1.0,logfile='',verbose='no',debug='no',mode='al')

#Producing a wavelength-calibrated arc spectrum, one way for user to check whether cal looks reasonable
#(just extracting a single row approximately down the middle of each order, then combining orders)
col = [88,77,65,54,53,92]
for i in range (1,7):
    try:
        iraf.imcopy(input='tftfarc_comb[sci,'+str(i)+']['+str(col[i-1])+',*]',output='tftfarc_comb_'+str(i),verbose='yes',mode='ql')
    except (TypeError,iraf.IrafError) as e:
        print( e)
        print( "XDGNIRS: We seem to have a problem; exiting gracefully...")
        print( "XDGNIRS ERROR: Problem in wavelength calibration/transformation step. First, check that your raw and reduced arcs look sensible. Second, check that the spectral orders are in the same place in the array (in the x direction) in all the raw files to within a few pixels. Third, try running nssdist, nsfitcoords, and nswavelength interactively. The examples on this web page may be helpful: http://www.gemini.edu/sciops/instruments/gnirs/data-format-and-reduction/reducing-xd-spectra#wavcal")
        print( "")
        sys.exit(0)
        
iraf.odcombine(input='tftfarc_comb_1,tftfarc_comb_2,tftfarc_comb_3,tftfarc_comb_4,tftfarc_comb_5,tftfarc_comb_6',output='calibrated_arc',headers='',bpmasks='',rejmasks='',nrejmasks='',expmasks='',sigmas='',logfile='STDOUT',apertures='',group='all',first='no',w1='INDEF',w2='INDEF',dw='INDEF',nw='INDEF',log='no',combine='average',reject='none',outtype='real',outlimits='',smaskformat='bpmspectrum',smasktype='none',smaskvalue=0.0,blank=0.0,scale='none',zero='none',weight='none',statsec='',expname='',lthreshold='INDEF',hthreshold='INDEF',nlow=1,nhigh=1,nkeep=1,mclip='yes',lsigma=3.0,hsigma=3.0,rdnoise='0.0',gain='1.0',snoise='0.0',sigscale=0.1,pclip=-0.5,grow=0.0,offsets='physical',masktype='none',maskvalue=0.0,mode='al')

#Also, check that the start and end wavelengths of the orders are correct 
#As of Jan 2016, we advertise that "wavelength settings ... are accurate to better than 5 percent of the wavelength coverage."
#Note (a) on this web page: http://www.gemini.edu/sciops/instruments/gnirs/spectroscopy
#So, take the nominal start and end wavelengths from the OT, and see if the achieved ones are the same to within the advertised tolerance
#***ALL ASSUMES CENTRAL WAVELENGTH = 1.65 UM***
nominal_wavelengths = ([0, 0], [18690,25310], [14020, 18980], [11220, 15180], [9350, 12650], [8020, 10840], [7020, 9480]) 
for i in range (1,7):
    wstart = iraf.hselect(images='tftfarc_comb[sci,'+str(i)+']', fields='CRVAL2', expr='yes', missing='INDEF', Stdout=1)
    wstart = float(wstart[0].replace("'",""))
    wdelt = iraf.hselect(images='tftfarc_comb[sci,'+str(i)+']', fields='CDELT2', expr='yes', missing='INDEF', Stdout=1)
    wdelt = float(wdelt[0].replace("'",""))
    wend = wstart + (1022 * wdelt)
    advertised_accuracy = (nominal_wavelengths[i][1] - nominal_wavelengths[i][0]) * 0.05
    if abs(wstart - nominal_wavelengths[i][0]) > advertised_accuracy:
        print( "***WARNING: Start wavelength for extension "+str(i)+" is >5% away from expected value (actual "+str(wstart)+" um vs expected "+str(nominal_wavelengths[i][0])+"+/- "+str(advertised_accuracy)+" um)")
    else:
        print( "***CHECK: Start wavelength for extension "+str(i)+" is <5% away from expected value (actual "+str(wstart)+" um vs expected "+str(nominal_wavelengths[i][0])+"+/- "+str(advertised_accuracy)+" um)")
    if abs(wend - nominal_wavelengths[i][1]) > advertised_accuracy:
        print( "***WARNING: End wavelength for extension "+str(i)+" is >5% away from expected value (actual "+str(wend)+" um vs expected "+str(nominal_wavelengths[i][1])+"+/- "+str(advertised_accuracy)+" um)")
    else:

        print( "***CHECK: End wavelength for extension "+str(i)+" is <5% away from expected value (actual "+str(wend)+" um vs expected "+str(nominal_wavelengths[i][1])+"+/- "+str(advertised_accuracy)+" um)")
          
#Now need to run nsfitcoords on science target and standard star data (all files); also un-sky-subtracted but otherwise reduced sky files (if creating an error spectrum)
#first w/ pinholes (to straighten) and then w/ arc (to de-tilt)
#Seems pointless to run nsfitcoords on each individual spectrum;
#All nsfitcoords does is write database info, and depends only on arc and pinholes, not on-sky data
#So, adding a hack to allow existing nsfitcoords solution (from above) to be used on science target and standard star files, w/o actually going through nsfitcoords
#This is done by adding some header keywords which (IIUC) tell nstransform which database files to use

def hedit_and_transform (whichfiles, prefix):
    print('####')
    print('####')
    print( whichfiles)
    print('####')
    for i in range (0, len(whichfiles)):
        whichfiles[i] = whichfiles[i].replace('\n','')
        input = prefix+whichfiles[i]
        iraf.copy (input=input,output='f'+input,verbose="no",mode="ql")
        #need to edit PHU *and* science extensions
        #first, add sdist info and transform the files to make the orders vertical
        iraf.hedit (images='f'+input+'[0]',fields="NSFITCOO",value="copied from arc",add="yes",addonly='no',delete='no',verify="no",show="no",update='yes',mode='al')
        for j in range(1, 7):
            iraf.hedit (images='f'+input+'[sci,' + str(j) +']', field="FCFIT2", value="farc_comb_SCI_" + str(j) + "_sdist", add="yes",addonly='no',delete='no', verify="no", show="no",update='yes',mode='al')
        iraf.nstransform(inimages='f'+input,outspectra='',outprefix='t',dispaxis=1,database='',fl_stripe='no',interptype='poly3',xlog='no',ylog='no',pixscale=1.0,logfile='',verbose='yes',debug='no',mode='al')
        #next, add the lamp info and transform the files to make the sky lines horizontal (spatial direction along detector rows)
        for j in range(1, 7):
            iraf.hedit (images='tf'+input+'[sci,' + str(j) +']', field="FCFIT1", value="ftfarc_comb_SCI_" + str(j) + "_lamp", add="yes",addonly='no',delete='no', verify="no", show="no",update='yes',mode='al')
        iraf.nstransform(inimages='tf'+input,outspectra='',outprefix='t',dispaxis=1,database='',fl_stripe='no',interptype='poly3',xlog='no',ylog='no',pixscale=1.0,logfile='',verbose='yes',debug='no',mode='al')
#        #while we're at it, create lists to feed into nscombine
#        if i == 0:
#            list = 'ttf'+input
#        else:
#            list += ',ttf'+input
#    return list

target = open(sys.argv[3:4][0]).readlines()
standard = open(sys.argv[4:5][0]).readlines()
target_list = hedit_and_transform(target, "rlnc")
standard_list = hedit_and_transform(standard, "rlnc")






