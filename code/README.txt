
++++++++++  To Install  ++++++++++

Include in your .bashrc: 

### GNIRS data reduction:
XDGNIRSDIR={PATH TO}/XDGNIRS_v2.2/
export XDGNIRSDIR
alias xdpiped="csh ${XDGNIRSDIR}/XDpiped.csh"

Include in your .cshrc: 

### GNIRS data reduction:
setenv XDGNIRSDIR   {PATH TO}/XDGNIRS_v2.2/
alias xdpiped "csh ${XDGNIRSDIR}/XDpiped.csh"


++++++++++  For Help  ++++++++++

xdpiped -h


++++++++++  Manual  ++++++++++

PATH/TO/XDGNIRS_v2.2/doc/XDGNIRS_v22.pdf

