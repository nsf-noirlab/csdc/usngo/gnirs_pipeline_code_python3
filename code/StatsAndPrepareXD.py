#!/usr/bin/env python
#
##
##	StatsandPrepare.py:   		
##
##

import sys
from pyraf import *
import pyfits
import numpy as np
from pyfits import getdata, getheader
#iraf.reset(gemini="extern$gemini_public_1_11_1/")
iraf.gemini(_doprint=0, motd="no")  
iraf.gnirs(_doprint=0)
iraf.imutil(_doprint=0)
iraf.nsheaders('gnirs',Stdout='/dev/null')


nqh = int(sys.argv[4:5][0])
nir = int(sys.argv[5:6][0])
calcstats = sys.argv[6:7][0]

infile = sys.argv[1:2][0]
pref = sys.argv[2:3][0]
file = open(infile,'r')
lines = file.readlines()

j = 1
listaA = listaB = listaC = ''
allOK = True
for line in lines:
    line = line.replace('\n','')
    aux = line.split('\t')
    fileName = str(aux[0])
    aux = fileName.split('.')
    fileName = str(aux[0])	
    if j == 1:			
        listaA += 'c'+fileName+'[1]'
        listaB += 'c'+fileName
        listaC += 'nc'+fileName
    else:
        listaA += ',c'+fileName+'[1]'
        listaB += ',c'+fileName
        listaC += ',nc'+fileName
    j = int(j) +1
	#Check difference in stats between raw/cleaned files and warn if large differences found
    mean_raw = str(iraf.imstatistic(images=line+'[1]',fields='mean',lower="INDEF",upper="INDEF",nclip=0,lsigma=3.0,usigma=3.0,binwidth=0.1,format='yes',cache='no',mode='al',Stdout=1))
    mean_raw = float(mean_raw.split()[3].replace("']",""))
    mean_clean = str(iraf.imstatistic(images='c'+line+'[1]',fields='mean',lower="INDEF",upper="INDEF",nclip=0,lsigma=3.0,usigma=3.0,binwidth=0.1,format='yes',cache='no',mode='al',Stdout=1))
    mean_clean = float(mean_clean.split()[3].replace("']",""))
    if abs(mean_raw - mean_clean) > mean_raw * 0.1:
        print( "***WARNING: Counts in cleaned image differ by >10% from those in the raw data (file", line, ")")
        allOK = False
if allOK:
    print( "***CHECK: Counts in cleaned images differ by <10% from those in the raw data")

#Record stats on cleaned files (--> XDGNIRS_Log and Checks_and_Warnings files)
#If cleaning not done, are actually recording stats on raw data (symbolic link from cN* to N*)
#Only reason for running this twice is to get stats written into log files with readable formatting...

iraf.imstatistic(images=listaA,fields='image,npix,mean,stddev,min,max',lower="INDEF",upper="INDEF",nclip=0,lsigma=3.0,usigma=3.0,binwidth=0.1,format='yes',cache='no',mode='al')
stats = iraf.imstatistic(images=listaA,fields='mean',lower="INDEF",upper="INDEF",nclip=0,lsigma=3.0,usigma=3.0,binwidth=0.1,format='yes',cache='no',mode='al', Stdout=1)

qh_mean = []
ir_mean = []
if calcstats == "yes":
    for i in range (1,nqh+1):
        qh_mean.append(float(str(stats[i]).split()[0]))
    for i in range (nqh+1, nqh+nir+1):
        ir_mean.append(float(str(stats[i]).split()[0]))
	#find mean of counts in individual flats, check for "unusual" values
	#These ranges are just rough guesses, may need refining later
    qh_overall_mean = np.mean(qh_mean)
    if qh_overall_mean > 450 or qh_overall_mean < 340:
        print( "***WARNING: Unusual counts in QH flats (mean >450 or <340). Is everything OK?")
    else:
        print( "***CHECK: Mean counts in QH flats looks normal (350 < mean < 450)")
    ir_overall_mean = np.mean(ir_mean)
    if ir_overall_mean > 250 or ir_overall_mean < 150:
        print( "***WARNING: Unusual counts in IR flats (mean >250 or <140). Is everything OK?")
    else:
        print( "***CHECK: Mean counts in IR flats looks normal (140 < mean < 250)")
	#check for flats deviating from mean by > 10%
    for i in range (0, nqh):
        allOK=True
        if (abs(qh_mean[i] - qh_overall_mean) > (qh_overall_mean * 0.1)):
            print( "***WARNING: QH flat(s) with deviant counts detected; do you need to reject some data?")
            allOK = False
    if allOK:
        print( "***CHECK: No deviant QH flats detected")
    for i in range (0, nir):
        allOK=True
        if (abs(ir_mean[i] - ir_overall_mean) > (ir_overall_mean * 0.1)):
            print( "***WARNING: IR flat(s) with deviant counts detected; do you need to reject some data?")
            allOK = False
    if allOK:    
        print( "***CHECK: No deviant IR flats detected")

#want to use the most appropriate bad pixel mask (BPM; although I'm not sure how much difference it will make)
#--> before the summer 2012 lens replacement, use gnirsn_2011apr07_bpm.fits
#--> after summer 2012, use gnirsn_2012dec05_bpm.fits 
#SB data before/after lens replacement distinguished by CAMERA header keyword (ShortBlue_G5538 vs ShortBlue_G5540)
#However, may want to generalise code for other cameras in future so use ARRAYID keyword instead (also changed before/after that eng. work)

config_dir = sys.argv[7:8][0]
arrayID = iraf.hselect (images=listaB[0:15]+'[0]',field="ARRAYID",expr="yes",missing="INDEF",Stdout=1)

if str(arrayID[0]) == "SN7638228.1":
    bpm = config_dir+"gnirsn_2011apr07_bpm"
elif str(arrayID[0]) == "SN7638228.1.2":
    bpm = config_dir+"gnirsn_2012dec05_bpm.fits"
else:
    print( "ERROR: unknown array ID. XDGNIRS has not been tested on data from GNIRS at Gemini South. Exiting script.")

#Note that in both nsprepare and nsreduce, we're using local copies of some of the config files 
#v1.11.1 users processing post-lens replacement data need new config files; seems easier to bundle in one place than to figure out users' paths

iraf.nsprepare(inimages=listaB,rawpath='',outimages='',outprefix=pref,bpm=bpm,logfile='',fl_vardq='yes',fl_cravg='no',crradius=0,fl_dark_mdf='no',fl_correct='no',fl_saturated='yes',fl_nonlinear='yes',fl_checkwcs='yes',fl_forcewcs='yes',arraytable=config_dir+'array.fits',configtable=config_dir+'config.fits',specsec='[*,*]',offsetsec='none',pixscale='0.15',shiftimage='',shiftx='INDEF',shifty='INDEF',obstype='FLAT',fl_inter='no',verbose='yes',mode='al')

#nsreduce parameters are slightly different depending on Gemini IRAF version used
gemiraf = sys.argv[3:4][0]
if gemiraf == "1.11.1":
    iraf.nsreduce(inimages=listaC,outimages='',outprefix ="r",fl_nscut='yes',section="",fl_corner='yes',fl_process_c='yes',fl_nsappwave='no',nsappwavedb=config_dir+'nsappwave.fits',crval='INDEF',cdelt='INDEF',fl_dark='no',darkimage='',fl_save_dark='no',fl_sky='no',skyimages='',skysection='',combtype='median',rejtype='avsigclip',masktype='goodvalue',maskvalue=0.0,scale='none',zero='median',weight='none',statsec='[*,*]',lthreshold='INDEF',hthreshold='INDEF',nlow=1,nhigh=1,nkeep=0,mclip='yes',lsigma=3.0,hsigma=3.0,snoise='0.0',sigscale=0.1,pclip=-0.5,grow=0.0,skyrange='INDEF',nodsize=3.0,fl_flat='no',flatimage='',flatmin=0.0,fl_vardq='yes',logfile='',verbose='yes',debug='no',force='no',mode='al')
else:
    iraf.nsreduce(inimages=listaC,outimages='',outprefix ="r",fl_cut='yes',section="",fl_corner='yes',fl_process_c='yes',fl_nsappwave='no',nsappwavedb=config_dir+'nsappwave.fits',crval='INDEF',cdelt='INDEF',fl_dark='no',darkimage='',fl_save_dark='no',fl_sky='no',skyimages='',skysection='',combtype='median',rejtype='avsigclip',masktype='goodvalue',maskvalue=0.0,scale='none',zero='median',weight='none',statsec='[*,*]',lthreshold='INDEF',hthreshold='INDEF',nlow=1,nhigh=1,nkeep=0,mclip='yes',lsigma=3.0,hsigma=3.0,snoise='0.0',sigscale=0.1,pclip=-0.5,grow=0.0,skyrange='INDEF',nodsize=3.0,fl_flat='no',flatimage='',flatmin=0.0,fl_vardq='yes',logfile='',verbose='yes',debug='no',force='no',mode='al')


