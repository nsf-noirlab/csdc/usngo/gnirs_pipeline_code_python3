#!/usr/bin/python
#
#Identifies and removes radiation events/cosmic rays/whatever spikes 
#Runs nsreduce to cut, flatfield, and sky-subtract the data
#Very early code that could do with being written with many fewer lines...
#
import sys
from pyraf import *
import pyfits
import numpy
from pyfits import getdata, getheader
#iraf.reset(gemini="extern$gemini_public_1_11_1/")
iraf.gemini(_doprint=0, motd="no")
iraf.gnirs(_doprint=0)
iraf.imutil(_doprint=0)
iraf.nsheaders('gnirs',Stdout='/dev/null')
iraf.unlearn(iraf.jimexam)

infile = sys.argv[1:2]
infile = infile[0]

nameoutput1 =  infile.split('.')
nameoutput1 = str(nameoutput1[0])
thresh = sys.argv[3:4][0]
file = open(infile,'r')
lines = file.readlines()
j = 1
listaA = listaB = listaC = listaD = listaE = listaF = listaG = ''
for line in lines:
    line = line.replace('\n','')
    aux = line.split('\t')
    fileName = str(aux[0])
    aux = fileName.split('.')
    fileName = str(aux[0])    
    if j == 1:            
        listaA += 'lnc'+fileName
        listaB += 'nc'+fileName
        listaC += 'msk'+fileName
        listaD += 'gmsk'+fileName
        listaE += 'bgmsk'+fileName
        listaF += 'rlnc'+fileName
        listaG += 'klnc'+fileName
        obs=pyfits.open('nc'+fileName+'.fits')
        rdnoise = obs[0].header["RDNOISE"]
        gain = obs[0].header["GAIN"]
    else:
        listaA += ',lnc'+fileName
        listaB += ',nc'+fileName
        listaC += ',msk'+fileName
        listaD += ',gmsk'+fileName
        listaE += ',bgmsk'+fileName
        listaF += ',rlnc'+fileName
        listaG += ',klnc'+fileName
    j = int(j) +1
kreject = int(j) - 2

#Delete existing masks and reduced files ("A" beam only)
#--> dev/null so don't get loads of warnings if files don't yet exist
iraf.imdelete(listaA,verify='no',Stderr='/dev/null')
iraf.imdelete(listaC,verify='no',Stderr='/dev/null')
iraf.imdelete(listaD,verify='no',Stderr='/dev/null')
iraf.imdelete(listaE,verify='no',Stderr='/dev/null')
iraf.imdelete(listaF,verify='no',Stderr='/dev/null')
iraf.imdelete(listaG,verify='no',Stderr='/dev/null')
#Create relatively clean minimum image from "A" beam data
iraf.gemcombine(input=listaB,output=nameoutput1,title="",combine="median",reject="minmax",offsets = "none",masktype="goodvalue",maskvalue = 0.,scale = "none",zero = "none",weight = "none",statsec = "[*,*]",expname="EXPTIME",lthreshold = 'INDEF',hthreshold = 'INDEF',nlow=0,nhigh=kreject,nkeep = 1,mclip = 'yes',lsigma = 3., hsigma = 3.,key_ron = "RDNOISE", key_gain = "GAIN",ron = 0.0,gain = 1.0,snoise="0.0",sigscale=0.1,pclip = -0.5,grow = 0.0,bpmfile = "",nrejfile = "",sci_ext = "SCI",var_ext = "VAR",dq_ext = "DQ",fl_vardq='yes',logfile = "gemcombine.log",fl_dqprop = 'no',verbose = 'yes')

#Create initial masks, "A" beam data
j = 1
for line in lines:
    line = line.replace('\n','')
    aux = line.split('\t')
    fileName = str(aux[0])
    aux = fileName.split('.')
    fileName = str(aux[0])
    iraf.imexpr("(a-b)>"+thresh+"*sqrt("+str(rdnoise)+"**2+2*b/"+str(gain)+") ? 1 : 0",output='msk'+fileName+'.pl',a='nc'+fileName+'.fits[SCI]',b=nameoutput1+'.fits[SCI]',dims = "auto",intype = "int",outtype = "auto",refim = "auto",bwidth = 0,btype = "nearest",bpixval = 0.,rangecheck = 'yes',verbose = 'yes',exprdb = "none")
    j = int(j) +1


#Now create min/masks for the off-source/beam B data
infile = sys.argv[2:3][0]
nameoutput2 =  infile.split('.')
nameoutput2 = str(nameoutput2[0])
file = open(infile,'r')
lines = file.readlines()
j = 1
listaB = delA = delC = delD = delE = delF = delG = ''
for line in lines:
    line = line.replace('\n','')
    aux = line.split('\t')
    fileName = str(aux[0])
    aux = fileName.split('.')
    fileName = str(aux[0])    
    if j == 1:            
        listaB += 'nc'+fileName
        #because of way lists originally constructed, need to make new lists for deleting existing "B" beam files
        delA += 'lnc'+fileName
        delC += 'msk'+fileName
        delD += 'gmsk'+fileName
        delE += 'bgmsk'+fileName
        delF += 'rlnc'+fileName
        delG += 'klnc'+fileName
    else:
        listaB += ',nc'+fileName
    listaA += ',lnc'+fileName
    listaC += ',msk'+fileName
    listaD += ',gmsk'+fileName
    listaE += ',bgmsk'+fileName
    delA += ',lnc'+fileName
    delC += ',msk'+fileName
    delD += ',gmsk'+fileName
    delE += ',bgmsk'+fileName
    delF += ',rlnc'+fileName
    delG += ',klnc'+fileName
    j = int(j) +1

iraf.imdelete(delA,verify='no',Stderr='/dev/null')
iraf.imdelete(delC,verify='no',Stderr='/dev/null')
iraf.imdelete(delD,verify='no',Stderr='/dev/null')
iraf.imdelete(delE,verify='no',Stderr='/dev/null')
iraf.imdelete(delF,verify='no',Stderr='/dev/null')
iraf.imdelete(delG,verify='no',Stderr='/dev/null')

kreject = int(j) - 2
iraf.gemcombine(input=listaB,output=nameoutput2,combine="median",reject="minmax",nhigh=kreject,nlow=0,masktype="goodvalue",fl_vardq='yes',title="",offsets = "none",maskvalue = 0.,scale = "none",zero = "none",weight = "none",statsec = "[*,*]",expname = "EXPTIME",lthreshold = 'INDEF',hthreshold = 'INDEF',nkeep = 1,mclip = 'yes',lsigma = 3., hsigma = 3.,key_ron = "RDNOISE", key_gain = "GAIN", ron = 0.0, gain = 1.0 ,snoise = "0.0",sigscale = 0.1,pclip = -0.5,grow = 0.0,bpmfile = "",nrejfile = "",sci_ext = "SCI",var_ext = "VAR",dq_ext = "DQ",logfile = "gemcombine.log",fl_dqprop = 'no',verbose = 'yes')

infile = sys.argv[2:3]
infile = infile[0]
nameoutput2 =  infile.split('.')
nameoutput2 = str(nameoutput2[0])
file = open(infile,'r')
lines = file.readlines()
j = 1
for line in lines:
    line = line.replace('\n','')
    aux = line.split('\t')
    fileName = str(aux[0])
    aux = fileName.split('.')
    fileName = str(aux[0])    
    iraf.imexpr("(a-b)>"+thresh+"*sqrt("+str(rdnoise)+"**2+2*b/"+str(gain)+") ? 1 : 0",output='msk'+fileName+'.pl',a='nc'+fileName+'.fits[SCI]',b=nameoutput2+'.fits[SCI]',dims = "auto",intype = "int",outtype = "auto",refim = "auto",bwidth = 0,btype = "nearest",bpixval = 0.,rangecheck = 'yes',verbose = 'yes',exprdb = "none")
    j = int(j) +1
iraf.crgrow(input=listaC,output=listaD,radius=1.5,inval = "INDEF",outval = "INDEF")

#Interpolate over the radiation events using fixpix 
#have chosen to interpolate along lines to avoid bad effects when a radiation event is right on a sky line
#column interpolation may be better for events landing on peak of target spectra, but effects from bad interpolation over sky lines seem worse
#"A" beam
infile = sys.argv[1:2]
infile = infile[0]
file = open(infile,'r')
lines = file.readlines()
j = 1
for line in lines:
    line = line.replace('\n','')
    aux = line.split('\t')
    fileName = str(aux[0])
    aux = fileName.split('.')
    fileName = str(aux[0])    
    iraf.copy('nc'+fileName+'.fits','lnc'+fileName+'.fits',verbose = 'no') 
    #ext [4] is the DQ plane added by nsprepare. Adding this to the mask adds bad pixels from the BPM, so they can be fixed as well
    iraf.imarith('gmsk'+fileName+'.pl',"+",nameoutput1+'[4].fits','bgmsk'+fileName+'.pl',title = "",divzero = 0.,hparams = "",pixtype = "", calctype = "",verbose = 'no',noact = 'no')
    iraf.proto.fixpix(images='lnc'+fileName+'[sci,1]',masks='bgmsk'+fileName+'.pl',linterp = 1, cinterp = "INDEF",verbose = 'no',pixels = 'no')

#"B" beam
infile = sys.argv[2:3]
infile = infile[0]
file = open(infile,'r')
lines = file.readlines()
j = 1
for line in lines:
    line = line.replace('\n','')
    aux = line.split('\t')
    fileName = str(aux[0])
    aux = fileName.split('.')
    fileName = str(aux[0])    
    iraf.copy('nc'+fileName+'.fits','lnc'+fileName+'.fits',verbose = 'no') 
    iraf.imarith('gmsk'+fileName+'.pl',"+",nameoutput2+'[4].fits','bgmsk'+fileName+'.pl',title = "",divzero = 0.,hparams = "",pixtype = "", calctype = "",verbose = 'no',noact = 'no')
    iraf.proto.fixpix(images='lnc'+fileName+'[sci,1]',masks='bgmsk'+fileName+'.pl',linterp = 1, cinterp = "INDEF",verbose = 'no',pixels = 'no')

#Reduce the radiation event-cleaned data (flatfield, sky subtract, ...); all files (both beams)
#Also reduce the data without sky subtraction, so can use sky frames for noise calculation later (if desired)
#Should really redo all that list construction so can easily ID sky frames and only reduce those w/o sky subtraction...
#nsreduce parameters slightly different between Gemini IRAF 1.11.1 and 1.12beta
gemiraf = sys.argv[4:5][0]
configdir = sys.argv[5:6][0]
errorspec = sys.argv[6:7][0]

if gemiraf == "1.11.1":
           iraf.nsreduce(inimages=listaA,outprefix ="r",fl_sky='yes',skyrange='INDEF',fl_flat='yes',fl_nsappwave='no',flatimage='MasterFlat.fits',outimages = "",fl_nscut = 'yes',section = "",fl_corner = 'yes',fl_process_cut = 'yes',nsappwavedb = configdir+"config/nsappwave.fits",crval = "INDEF",cdelt = "INDEF",fl_dark = 'no',darkimage = "",fl_save_dark = 'no',skyimages = "",skysection = "",combtype = "median",rejtype = "avsigclip",masktype = "goodvalue",maskvalue = 0.,scale = "none",zero = "median",weight = "none",statsec = "[*,*]",lthreshold = "INDEF", hthreshold = "INDEF",nlow = 1,  nhigh = 1,nkeep = 0,mclip = 'yes',lsigma = 3., hsigma = 3.,snoise = "0.0",sigscale = 0.1,pclip = -0.5,grow = 0.0,nodsize = 3.,flatmin = 0.0,fl_vardq = 'yes',logfile = "",verbose = 'yes',debug = 'no',force = 'no')
           if errorspec == 'yes':
                iraf.nsreduce(inimages=listaA,outprefix ="k",fl_sky='no',skyrange='INDEF',fl_flat='yes',fl_nsappwave='no',flatimage='MasterFlat.fits',outimages = "",fl_nscut = 'yes',section = "",fl_corner = 'yes',fl_process_cut = 'yes',nsappwavedb = configdir+"config/nsappwave.fits",crval = "INDEF",cdelt = "INDEF",fl_dark = 'no',darkimage = "",fl_save_dark = 'no',skyimages = "",skysection = "",combtype = "median",rejtype = "avsigclip",masktype = "goodvalue",maskvalue = 0.,scale = "none",zero = "median",weight = "none",statsec = "[*,*]",lthreshold = "INDEF", hthreshold = "INDEF",nlow = 1,  nhigh = 1,nkeep = 0,mclip = 'yes',lsigma = 3., hsigma = 3.,snoise = "0.0",sigscale = 0.1,pclip = -0.5,grow = 0.0,nodsize = 3.,flatmin = 0.0,fl_vardq = 'yes',logfile = "",verbose = 'yes',debug = 'no',force = 'no')

else:
           iraf.nsreduce(inimages=listaA,outprefix ="r",fl_sky='yes',skyrange='INDEF',fl_flat='yes',fl_nsappwave='no',flatimage='MasterFlat.fits',outimages = "",fl_cut = 'yes',section = "",fl_corner = 'yes',fl_process_cut = 'yes',nsappwavedb = configdir+"config/nsappwave.fits",crval = "INDEF",cdelt = "INDEF",fl_dark = 'no',darkimage = "",fl_save_dark = 'no',skyimages = "",skysection = "",combtype = "median",rejtype = "avsigclip",masktype = "goodvalue",maskvalue = 0.,scale = "none",zero = "median",weight = "none",statsec = "[*,*]",lthreshold = "INDEF", hthreshold = "INDEF",nlow = 1,  nhigh = 1,nkeep = 0,mclip = 'yes',lsigma = 3., hsigma = 3.,snoise = "0.0",sigscale = 0.1,pclip = -0.5,grow = 0.0,nodsize = 3.,flatmin = 0.0,fl_vardq = 'yes',logfile = "",verbose = 'yes',debug = 'no',force = 'no') 
           if errorspec == 'yes':
               iraf.nsreduce(inimages=listaA,outprefix ="k",fl_sky='no',skyrange='INDEF',fl_flat='yes',fl_nsappwave='no',flatimage='MasterFlat.fits',outimages = "",fl_cut = 'yes',section = "",fl_corner = 'yes',fl_process_cut = 'yes',nsappwavedb = configdir+"config/nsappwave.fits",crval = "INDEF",cdelt = "INDEF",fl_dark = 'no',darkimage = "",fl_save_dark = 'no',skyimages = "",skysection = "",combtype = "median",rejtype = "avsigclip",masktype = "goodvalue",maskvalue = 0.,scale = "none",zero = "median",weight = "none",statsec = "[*,*]",lthreshold = "INDEF", hthreshold = "INDEF",nlow = 1,  nhigh = 1,nkeep = 0,mclip = 'yes',lsigma = 3., hsigma = 3.,snoise = "0.0",sigscale = 0.1,pclip = -0.5,grow = 0.0,nodsize = 3.,flatmin = 0.0,fl_vardq = 'yes',logfile = "",verbose = 'yes',debug = 'no',force = 'no') 

#Record threshold value in reduced files
for item in listaA.split(','):
    iraf.hedit(images='r'+item, fields='RTHRESH', value=thresh, add='yes', addonly='no', delete='no', verify='no', show='no', update='yes')




