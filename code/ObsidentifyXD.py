#!/usr/bin/python
#
#

import sys
from pyraf import *
import pyfits

infile = sys.argv[1:2][0]
infilen = sys.argv[2:3][0]
phase = sys.argv[3:4]
obsclassSel = sys.argv[4:5]

necessary_keywords = (['OBSTYPE','OBSCLASS','FILTER1','FILTER2','GRATING','SLIT','RA','DEC','POFFSET','QOFFSET','EXPTIME','COADDS','INSTRUME', 'OBJECT', 'PRISM'])

file = open(infile,'r')
fileout1 = open('LISTS/ID'+infilen+'.lst','w')
i = 1
for line in file:
    line = line[0:19]
    #Check that each file in the input list file exists
    try:
        with open(line) as f:
            pass
    except IOError as e:
        print( 'ERROR: File', line, "is named in", infile, "but cannot be found. Please sort this out and start again")
        sys.exit()

    #Check that all the necessary header keywords exist
	#Won't catch everything, e.g missing "OBJECT" keyword if OBSTYPE=OBJECT
	#But that probably means other stuff is also missing so just put it last in necessary_keywords and hope for the best
    header = str(iraf.imhead(images=line+'[0]',imlist="",long="yes",userfields="yes",Stdout=1))
    for thing in necessary_keywords:
        if thing not in header:
            print( "ERROR: Header keyword '",thing,"' appears to be missing from file",line,". You will need to either exclude this file from your input list, or - if you really know what you're doing - fix the header")
            sys.exit(0)

    #check that some of the keywords have sensible values        
    obs=pyfits.open(line)
    INSTRUME = obs[0].header["INSTRUME"]
    if INSTRUME != "GNIRS":
        print( "ERROR: File", line, "is not a GNIRS file; please remove it from", infile, "and start again")
        sys.exit(0)
    OBJECT = obs[0].header["OBJECT"]
    if OBJECT == "Dark":
        print( "ERROR: File", line, "is a dark; please remove it from", infile, "and start again")
        sys.exit(0)
    PRISM = obs[0].header["PRISM"] 
    if 'XD' not in PRISM:
        print( "ERROR: File", line, "appears not to contain cross-dispersed data; please remove it from", infile, "and start again. This may occasionally happen with acquisition images for XD spectra, if you used an unusual observational setup. In this case you could comment out lines 57 - 59 in ObsidentifyXD.py and try again.")
        sys.exit(0)
    LNRS =  obs[0].header["LNRS"]
    if LNRS == 0:
        print( "ERROR: header keyword LNRS = 0 in file", line, ". The GNIRS detector controller sometimes does this and it will cause problems with some of the IRAF tasks. It probably doesn't affect the data, though, so you could set LNRS to the correct value in the headers and restart the reduction.")
        sys.exit(0)

    OBSTYPE = obs[0].header["OBSTYPE"]
    OBSCLASS = obs[0].header["OBSCLASS"]
    FILTER1 = obs[0].header["FILTER1"]
    FILTER2 = obs[0].header["FILTER2"]
    GRATING = obs[0].header["GRATING"]
    SLIT = obs[0].header["SLIT"] 
    RA = obs[0].header["RA"]
    DEC = obs[0].header["DEC"]
    POFFSET = obs[0].header["POFFSET"]
    QOFFSET = obs[0].header["QOFFSET"]
    EXPTIME = obs[0].header["EXPTIME"]
    COADDS = obs[0].header["COADDS"]
    OBJECT="".join(OBJECT.split(" "))
    OBSTYPE="".join(OBSTYPE.split(" "))
    OBSCLASS="".join(OBSCLASS.split(" "))
    FILTER1="".join(FILTER1.split(" "))
    FILTER2="".join(FILTER2.split(" "))
    GRATING="".join(GRATING.split(" "))
    SLIT="".join(SLIT.split(" "))
    if OBSTYPE == "OBJECT":
        GCALLAMP = "none"
    else:
        GCALLAMP = obs[0].header["GCALLAMP"]
    GCALLAMP="".join(GCALLAMP.split(" "))
    TIMEOBS = obs[0].header["UT"]       
    DATEOBS = obs[0].header["DATE"]
    STARTTIME = float(TIMEOBS[0:2])*60.*60. + float(TIMEOBS[3:5])*60. +float(TIMEOBS[6:10])
    STARTDATE = float(DATEOBS[0:4])*12.*30. + float(DATEOBS[5:7])*30. +float(DATEOBS[8:11])
    START = (STARTDATE*24.*60.*60.)+STARTTIME
    fileout1.write(line + "\t" + OBJECT + "\t"+ OBSTYPE + "\t"+ OBSCLASS + "\t"+ str(EXPTIME) + "\t"+ FILTER1  + "\t"+ FILTER2 + "\t"+ GRATING + "\t"+ SLIT + "\t"+ GCALLAMP + "\t"+ str(RA) + "\t"+ str(DEC)  + "\t"+ str(POFFSET) + "\t"+ str(QOFFSET) + "\t" + str(START) + "\t" + str(COADDS) + "\n")
    i += 1
file.close()
fileout1.close()
