<img src="US_NGO_logo.jpeg"  width="400" height="400">

## **GNIRSXD Pipeline**
----

This repository contains the GNIRSXD pipeline, updated to work with Python 3.

The PyFits package needs to be installed before running the pipeline. This can be done by entering the following command in your terminal 'pip install pyfits'.


### **Instructions for running the pipeline**
1. Move science and calibration data to the virtual machine.
2. Modify the input list so that the following data are included

   - #### ***Science target acquisition images***
     - Target names in headers (OBJECT keyword) must be identical to target name in spectroscopy file headers
     - This means that blind offset obsercations cannot be reduced without first editing the headers
     - Only the final acquisition image in the sequence is strictly necessary (the acquisition images are only needed becasue of the telescope position information in the file headers)
     #  
     
   - #### ***Science target spectroscopy files***
     - At least **two** object-sky pairs
     - ABBA, ABA, or ABAB nod pattern
     - Nod along slit of off to blank sky
     - Must contain a "q" offset (as well as or instead of "p" offset)
     #  
     
   - #### ***Standard star acquisition images***
     - Same criteria as for science target acquisition images
     #  
     
   - #### ***Standard star spectroscopy files***
     - **One** standard star observation only
     - Same criteria as for science target spectroscopy
     #  
     
   - #### ***One or more arcs***
   #  
   - #### ***A set of IR and QH flats***
   #  
   - #### ***One or more pinhole flats***
   #  

Note: The code can currently only deal with **one** standard star and **one** science target data set at a time.

3. Run the XDpiped.csh script using the following command
   - .//Path/To/Script/XDpiped.csh inputfiles.lst

4. Inspect the final prodcuts that are located in the PRODUCTS directory

Note: Data taken before the 2012 lens replacement may contain many radiation events. To remedy this, make sure to set the despike_std parameter to 'yes' when running the pipeline.

---
## Need help?

Problems, comments, suggestions, and/or need help setting up and running the Jupyter notebooks? You can contact the US NGO members via our [Portal](http://ast.noao.edu/csdc/usngo), [Twitter](https://twitter.com/usngo), or submit a *New issue* through GitHub.

For assistance with running the GNIRSXD pipeline, please submit a ticket to the [Gemini Helpdesk](https://www.gemini.edu/observing/helpdesk/submit-general-helpdesk-request).

Follow us on Twitter! <a href="https://twitter.com/usngo" target="_blank"><img src="https://badgen.net/twitter/follow/usngo"></a>

---
